<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::post('/validation','mpesa\mpesaController@validationURL');
Route::post('/confirmation','mpesa\mpesaController@confirmationURL');
Route::post('/callback', 'mpesa\mpesaController@callbackURL');
Route::get('/mobilet', function() {
	  return File::get(public_path() . '/var/www/html/mobile/');
});
