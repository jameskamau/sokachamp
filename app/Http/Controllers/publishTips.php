<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\dailyTips;
use DB;
use DateTime;
class publishTips extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
	    //
	     $posts = dailyTips::orderBy('created_at','desc')->paginate(1);
	     return view('home', ["posts"=>$posts]);
	     
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    public function getDate(){
	    $date =new DateTime (date('Y-m-d'));
	    $todaysDate=$date->format('Y-m-d ');
	    return $todaysDate;
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
	    //
	    //
	    //
	    $this->validate(  $request,['dailyTips'=>'required']);
	   $postData= new dailyTips;
	    $postData->dailyTips=$request->input('dailyTips');
	   $postData->marketType=$request->input('marketType');
	    $postData->created=$this->getDate();
	   $postData->save();
	   return redirect('/home')->with('success', 'Tip Saves Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
