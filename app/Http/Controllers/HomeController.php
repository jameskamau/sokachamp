<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\dailyTips;
use DB;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(){
	$posts=dailyTips::orderby('created_at', 'desc')->paginate(1);    
        return view('home', ['posts'=>$posts]);
    }


    public function store(Request $request){
	     $postData= new dailyTips;
	     $postData->dailyTips=$request->input('dailyTip');
	     $postData->marketType=$request->input('marketType');
	     $postData->save();


    }
}
