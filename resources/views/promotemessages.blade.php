@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="card card-outline card-info">
            <div class="card-header">
              <h3 class="card-title">
                Enter Promotion Message
               
	      </h3>
	{!! Form::open(['action'=>'promoteMessages@store', 'method'=>'POST','enctype'=>'multipart/form-data'])!!}
              <!-- tools box -->
              <div class="card-tools">
                <button type="button" class="btn btn-tool btn-sm" data-card-widget="collapse" data-toggle="tooltip"
                        title="Collapse">
                  <i class="fas fa-minus"></i></button>
                <button type="button" class="btn btn-tool btn-sm" data-card-widget="remove" data-toggle="tooltip"
                        title="Remove">
                  <i class="fas fa-times"></i></button>
              </div>
              <!-- /. tools -->
            </div>
            <!-- /.card-header -->
            <div class="card-body pad">
              <div class="mb-3">
                <textarea class="textarea"  name="dailyTips" placeholder="Place some text here"
                          style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
              </div>
              <p class="text-sm mb-0">
               
		 <button type="submit" class="btn btn-block btn-primary">Promote Message</button>
		
              </p>
            </div>
          </div>
	</div>
        {!! Form::close() !!}
        <!-- /.col-->
      </div>
      <!-- ./row -->
    </section>

	<!--
       
            <div class="well">
                <div class="row">
                    
                    <div class="col-md-8 col-sm-8">
                       <h4></h4>
                        <small>sent to</small>
                    </div>
                </div>
            </div>
        
        
     -->
    


    
<section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="card card-outline card-info">
            <div class="card-header">
              <h3 class="card-title">
                Enter SMS MARKETING MESSAGE
               
	      </h3>
	{!! Form::open(['action'=>'smsMarketing@store', 'method'=>'POST','enctype'=>'multipart/form-data'])!!}
              <!-- tools box -->
              <div class="card-tools">
                <button type="button" class="btn btn-tool btn-sm" data-card-widget="collapse" data-toggle="tooltip"
                        title="Collapse">
                  <i class="fas fa-minus"></i></button>
                <button type="button" class="btn btn-tool btn-sm" data-card-widget="remove" data-toggle="tooltip"
                        title="Remove">
                  <i class="fas fa-times"></i></button>
              </div>
              <!-- /. tools -->
            </div>
            <!-- /.card-header -->
            <div class="card-body pad">
              <div class="mb-3">
                <textarea class="textarea"  name="marketingMessage" placeholder="Place some text here"
                          style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
	      </div>

		 <div class="card card-danger">
			<div class="card-header">
			<h3 class="card-title">Promotion Number Index range</h3>
			</div>
			<div class="card=body">
			<div class="row">
			    <div class="col-3">
				<input type="text" class="form-control" name="fromIndex">	
			    </div>
				
			<div class="col-3">
			   <input type="text" class="form-control" name="toIndex">	
			</div>

				
			</div>
			</div>

		</div>
		<br><br><br>	
              <p class="text-sm mb-0">
               
		 <button type="submit" class="btn btn-block btn-primary">Market-SMS-Message</button>
		
              </p>
            </div>
          </div>
	</div>
        {!! Form::close() !!}
        <!-- /.col-->
      </div>
      <!-- ./row -->
    </section>

	 
   



                </div>
            </div>
        </div>
    </div>
</div>
@endsection
