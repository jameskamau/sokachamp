@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="card card-outline card-info">
            <div class="card-header">
              <h3 class="card-title">
                Enter Match Tips predictions
               
	      </h3>
	{!! Form::open(['action'=>'publishTips@store', 'method'=>'POST','enctype'=>'multipart/form-data'])!!}
              <!-- tools box -->
              <div class="card-tools">
                <button type="button" class="btn btn-tool btn-sm" data-card-widget="collapse" data-toggle="tooltip"
                        title="Collapse">
                  <i class="fas fa-minus"></i></button>
                <button type="button" class="btn btn-tool btn-sm" data-card-widget="remove" data-toggle="tooltip"
                        title="Remove">
                  <i class="fas fa-times"></i></button>
              </div>
              <!-- /. tools -->
	    </div>


	 <div class="form-group">
               
            <label for="exampleInputEmail1"> <h4>Market Type</h4> </label>
            <select class="form-control" name="marketType">
                <option>Half Time / Full Time </option>
                <option>Correct Score</option>
                <option>Over / Under</option>
                <option>Jackpot</option>
                 <option>Multi-bet</option>
              </select>
                           
        </div>	


            <!-- /.card-header -->
            <div class="card-body pad">
              <div class="mb-3">
                <textarea class="textarea"  name="dailyTips" placeholder="Place some text here"
                          style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
              </div>
              <p class="text-sm mb-0">
               <!-- Editor <a href="https://github.com/summernote/summernote">Documentation and license
		information.</a> -->
		 <button type="submitn" class="btn btn-block btn-primary">Publish Tips</button>
		
              </p>
            </div>
          </div>
	</div>
        {!! Form::close() !!}
        <!-- /.col-->
      </div>
      <!-- ./row -->
    </section>


	
    @if(count($posts) > 0)
        @foreach($posts as $post)
            <div class="well">
                <div class="row">
                    
                    <div class="col-md-8 col-sm-8">
                        <h4>{{$post->dailyTips}}</h4>
                        <small>Written on {{$post->created_at}}</small>
                    </div>
                </div>
            </div>
        @endforeach
        
    @else
        <p>No posts found</p>
    @endif 
    



                </div>
            </div>
        </div>
    </div>
</div>
@endsection
