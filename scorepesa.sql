-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 01, 2020 at 05:15 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.3.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `scorepesa`
--

-- --------------------------------------------------------

--
-- Table structure for table `account_freeze`
--

CREATE TABLE `account_freeze` (
  `account_freeze_id` bigint(20) NOT NULL,
  `msisdn` varchar(50) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `airtel_money`
--

CREATE TABLE `airtel_money` (
  `id` bigint(20) NOT NULL,
  `msisdn` varchar(30) NOT NULL,
  `account_no` varchar(100) NOT NULL,
  `airtel_money_code` varchar(50) NOT NULL,
  `first_name` varchar(120) NOT NULL,
  `last_name` varchar(120) NOT NULL,
  `amount` decimal(10,2) NOT NULL,
  `time_stamp` datetime NOT NULL,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `airtel_money_rate`
--

CREATE TABLE `airtel_money_rate` (
  `id` bigint(20) NOT NULL,
  `min_amount` float NOT NULL,
  `max_amount` float NOT NULL,
  `charge` float NOT NULL,
  `created` datetime DEFAULT NULL,
  `updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `airtel_subs_blast`
--

CREATE TABLE `airtel_subs_blast` (
  `msisdn` varchar(40) NOT NULL,
  `id` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `android_auth`
--

CREATE TABLE `android_auth` (
  `id` int(11) NOT NULL,
  `profile_id` int(11) NOT NULL,
  `token` varchar(256) NOT NULL,
  `expiry` datetime NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `archive_bet`
--

CREATE TABLE `archive_bet` (
  `bet_id` int(11) NOT NULL,
  `profile_id` bigint(20) NOT NULL,
  `bet_message` text NOT NULL,
  `total_odd` decimal(10,2) NOT NULL,
  `bet_amount` decimal(10,2) NOT NULL,
  `tax` decimal(10,2) DEFAULT NULL,
  `taxable_possible_win` decimal(10,2) DEFAULT NULL,
  `raw_possible_win` decimal(10,2) DEFAULT NULL,
  `possible_win` decimal(10,2) NOT NULL,
  `status` smallint(1) NOT NULL,
  `win` tinyint(1) NOT NULL DEFAULT 0,
  `is_void` tinyint(1) DEFAULT NULL,
  `refund` decimal(10,2) DEFAULT NULL,
  `revised_possible_win` decimal(10,2) DEFAULT NULL,
  `reference` varchar(70) NOT NULL,
  `created_by` varchar(70) NOT NULL,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `archive_bet_slip`
--

CREATE TABLE `archive_bet_slip` (
  `bet_slip_id` int(11) NOT NULL,
  `parent_match_id` int(11) NOT NULL,
  `bet_id` int(11) NOT NULL,
  `bet_pick` varchar(250) DEFAULT NULL,
  `special_bet_value` varchar(20) NOT NULL DEFAULT '',
  `total_games` int(10) NOT NULL,
  `odd_value` decimal(10,2) NOT NULL,
  `win` tinyint(1) NOT NULL,
  `live_bet` tinyint(4) DEFAULT NULL,
  `void_factor` float DEFAULT 0,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT current_timestamp(),
  `status` int(1) NOT NULL,
  `sub_type_id` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `archive_bonus_bet`
--

CREATE TABLE `archive_bonus_bet` (
  `bonus_bet_id` int(11) NOT NULL,
  `bet_id` int(11) NOT NULL,
  `bet_amount` decimal(10,2) NOT NULL,
  `possible_win` decimal(10,2) NOT NULL,
  `profile_bonus_id` int(11) DEFAULT NULL,
  `won` tinyint(1) NOT NULL DEFAULT 0,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `ratio` decimal(10,2) NOT NULL,
  `created_by` varchar(70) NOT NULL,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `archive_bonus_trx`
--

CREATE TABLE `archive_bonus_trx` (
  `id` int(11) NOT NULL,
  `profile_id` bigint(20) NOT NULL,
  `account` varchar(50) NOT NULL,
  `iscredit` smallint(1) NOT NULL,
  `reference` varchar(50) NOT NULL,
  `amount` decimal(10,0) NOT NULL,
  `created_by` varchar(60) NOT NULL,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `profile_bonus_id` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `archive_jackpot_bet`
--

CREATE TABLE `archive_jackpot_bet` (
  `jackpot_bet_id` bigint(20) NOT NULL,
  `bet_id` int(11) NOT NULL,
  `jackpot_event_id` bigint(20) NOT NULL,
  `status` enum('ACTIVE','CANCELLED','FINISHED') NOT NULL DEFAULT 'ACTIVE',
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `archive_jackpot_trx`
--

CREATE TABLE `archive_jackpot_trx` (
  `jackpot_trx_id` bigint(20) NOT NULL,
  `trx_id` int(11) NOT NULL,
  `jackpot_event_id` bigint(20) NOT NULL,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `archive_jackpot_winner`
--

CREATE TABLE `archive_jackpot_winner` (
  `jackpot_winner_id` bigint(20) NOT NULL,
  `win_amount` decimal(10,2) NOT NULL,
  `bet_id` int(11) NOT NULL,
  `jackpot_event_id` bigint(20) NOT NULL,
  `msisdn` varchar(45) NOT NULL,
  `total_games_correct` int(11) NOT NULL,
  `created_by` varchar(70) NOT NULL,
  `status` int(11) DEFAULT 1,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `archive_match`
--

CREATE TABLE `archive_match` (
  `match_id` int(11) NOT NULL,
  `parent_match_id` int(11) NOT NULL,
  `home_team` varchar(50) NOT NULL,
  `away_team` varchar(50) NOT NULL,
  `start_time` datetime NOT NULL,
  `game_id` varchar(45) NOT NULL,
  `competition_id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `instance_id` int(10) NOT NULL DEFAULT 0,
  `bet_closure` datetime NOT NULL,
  `created_by` varchar(60) NOT NULL,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `result` varchar(45) DEFAULT NULL,
  `ht_score` varchar(5) DEFAULT NULL,
  `ft_score` varchar(5) DEFAULT NULL,
  `completed` tinyint(1) DEFAULT 0,
  `priority` int(10) DEFAULT 50,
  `ussd_priority` int(10) DEFAULT 50,
  `peer_priority` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `archive_transaction`
--

CREATE TABLE `archive_transaction` (
  `id` int(11) NOT NULL,
  `profile_id` bigint(20) NOT NULL,
  `account` varchar(50) NOT NULL,
  `iscredit` smallint(1) NOT NULL,
  `reference` varchar(50) NOT NULL,
  `amount` decimal(10,0) NOT NULL,
  `running_balance` decimal(10,2) DEFAULT NULL,
  `created_by` varchar(60) NOT NULL,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `status` enum('COMPLETE','PENDING') DEFAULT 'COMPLETE'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `arch_live_match`
--

CREATE TABLE `arch_live_match` (
  `match_id` int(11) NOT NULL,
  `parent_match_id` int(11) NOT NULL,
  `home_team` varchar(50) NOT NULL,
  `away_team` varchar(50) NOT NULL,
  `start_time` datetime NOT NULL,
  `game_id` varchar(20) NOT NULL,
  `competition_id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `instance_id` int(10) NOT NULL DEFAULT 0,
  `bet_closure` datetime NOT NULL,
  `created_by` varchar(60) NOT NULL,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `result` varchar(45) DEFAULT NULL,
  `ht_score` varchar(5) DEFAULT NULL,
  `ft_score` varchar(5) DEFAULT NULL,
  `completed` tinyint(1) DEFAULT 0,
  `priority` int(10) DEFAULT 50
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `auth_assignment`
--

CREATE TABLE `auth_assignment` (
  `item_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `auth_item`
--

CREATE TABLE `auth_item` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `type` int(11) NOT NULL,
  `description` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `rule_name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `auth_item_child`
--

CREATE TABLE `auth_item_child` (
  `parent` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `child` varchar(64) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `auth_rule`
--

CREATE TABLE `auth_rule` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `data` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `auth_token`
--

CREATE TABLE `auth_token` (
  `id` bigint(20) NOT NULL,
  `token` text DEFAULT NULL,
  `refresh_token` text DEFAULT NULL,
  `expire_seconds` int(11) NOT NULL DEFAULT 540 COMMENT 'I set this to 9 minutes since the token expiry time is rumoured to be 10 minutes',
  `max_refresh` tinyint(4) NOT NULL DEFAULT 10,
  `refresh_times` tinyint(4) NOT NULL DEFAULT 0,
  `token_type` enum('bulk','content') DEFAULT 'bulk',
  `time_received` timestamp NOT NULL DEFAULT current_timestamp(),
  `status` enum('active','expired','awaiting_refresh') NOT NULL DEFAULT 'active',
  `updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `backup_profile`
--

CREATE TABLE `backup_profile` (
  `profile_id` bigint(20) NOT NULL,
  `msisdn` varchar(45) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `status` smallint(1) DEFAULT NULL,
  `modified` timestamp NULL DEFAULT NULL,
  `created_by` varchar(45) DEFAULT NULL,
  `network` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bb`
--

CREATE TABLE `bb` (
  `bet_id` int(11) NOT NULL,
  `profile_id` bigint(20) NOT NULL,
  `bet_message` varchar(200) NOT NULL,
  `total_odd` decimal(10,2) NOT NULL,
  `bet_amount` decimal(10,2) NOT NULL,
  `possible_win` decimal(10,2) NOT NULL,
  `status` smallint(1) NOT NULL,
  `win` tinyint(1) NOT NULL DEFAULT 0,
  `reference` varchar(70) NOT NULL,
  `created_by` varchar(70) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bet`
--

CREATE TABLE `bet` (
  `bet_id` int(11) NOT NULL,
  `profile_id` bigint(20) NOT NULL,
  `bet_message` text NOT NULL,
  `total_odd` decimal(10,2) NOT NULL,
  `bet_amount` decimal(10,2) NOT NULL,
  `tax` decimal(10,2) DEFAULT NULL,
  `taxable_possible_win` decimal(10,2) DEFAULT NULL,
  `raw_possible_win` decimal(10,2) DEFAULT NULL,
  `possible_win` decimal(10,2) NOT NULL,
  `status` smallint(1) NOT NULL,
  `win` tinyint(1) NOT NULL DEFAULT 0,
  `is_void` tinyint(1) DEFAULT NULL,
  `refund` decimal(10,2) DEFAULT NULL,
  `revised_possible_win` decimal(10,2) DEFAULT NULL,
  `stake_tax` decimal(10,2) DEFAULT NULL,
  `reference` varchar(70) NOT NULL,
  `created_by` varchar(70) NOT NULL,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `betting_control`
--

CREATE TABLE `betting_control` (
  `id` int(11) NOT NULL,
  `disabled` tinyint(1) NOT NULL DEFAULT 0,
  `bookmaker_event` varchar(45) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `bet_discount`
--

CREATE TABLE `bet_discount` (
  `bet_discount_id` bigint(20) NOT NULL,
  `bet_id` bigint(20) NOT NULL,
  `discount_amount` decimal(10,2) NOT NULL,
  `ratio` decimal(10,2) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bet_slip`
--

CREATE TABLE `bet_slip` (
  `bet_slip_id` int(11) NOT NULL,
  `parent_match_id` int(11) NOT NULL,
  `bet_id` int(11) NOT NULL,
  `bet_pick` varchar(250) DEFAULT NULL,
  `special_bet_value` varchar(20) NOT NULL DEFAULT '',
  `total_games` int(10) NOT NULL,
  `odd_value` decimal(10,2) NOT NULL,
  `win` tinyint(1) NOT NULL,
  `live_bet` tinyint(4) DEFAULT NULL,
  `void_factor` float DEFAULT 0,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT current_timestamp(),
  `status` int(1) NOT NULL,
  `sub_type_id` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bet_slip_check`
--

CREATE TABLE `bet_slip_check` (
  `bet_slip_id` int(11) NOT NULL,
  `parent_match_id` int(11) NOT NULL,
  `bet_id` int(11) NOT NULL,
  `bet_pick` varchar(20) NOT NULL,
  `special_bet_value` varchar(20) NOT NULL DEFAULT '',
  `total_games` int(10) NOT NULL,
  `odd_value` decimal(10,2) NOT NULL,
  `win` tinyint(1) NOT NULL,
  `live_bet` tinyint(4) DEFAULT NULL,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT current_timestamp(),
  `status` int(1) NOT NULL,
  `sub_type_id` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bet_slip_temp`
--

CREATE TABLE `bet_slip_temp` (
  `bet_slip_id` int(11) NOT NULL,
  `match_id` int(11) NOT NULL,
  `bet_pick` varchar(20) NOT NULL,
  `reference_id` varchar(512) NOT NULL,
  `status` int(1) NOT NULL,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT current_timestamp(),
  `sub_type_id` int(11) DEFAULT NULL,
  `special_bet_value` varchar(20) NOT NULL DEFAULT '',
  `live_bet` tinyint(1) NOT NULL DEFAULT 0,
  `bet_type` varchar(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bet_status_changes`
--

CREATE TABLE `bet_status_changes` (
  `status_change_id` bigint(20) NOT NULL,
  `parent_match_id` bigint(20) NOT NULL,
  `bet_status` tinytext NOT NULL,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bleague_competition`
--

CREATE TABLE `bleague_competition` (
  `competition_id` int(11) NOT NULL,
  `competition_name` varchar(120) NOT NULL,
  `category` varchar(120) NOT NULL,
  `status` smallint(1) NOT NULL,
  `sport_id` int(11) NOT NULL,
  `created_by` varchar(70) NOT NULL,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `priority` int(10) DEFAULT 0,
  `max_stake` decimal(10,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bleague_event_odd`
--

CREATE TABLE `bleague_event_odd` (
  `event_odd_id` int(11) NOT NULL,
  `parent_match_id` int(11) DEFAULT NULL,
  `sub_type_id` int(11) DEFAULT NULL,
  `max_bet` decimal(10,0) NOT NULL,
  `special_bet_value` varchar(255) DEFAULT NULL,
  `created_by` varchar(45) NOT NULL,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `odd_key` varchar(200) NOT NULL,
  `odd_value` varchar(20) DEFAULT NULL,
  `odd_alias` varchar(20) DEFAULT NULL,
  `status` tinyint(4) DEFAULT 1,
  `approved_by` varchar(45) DEFAULT NULL,
  `winning_outcome` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bleague_match`
--

CREATE TABLE `bleague_match` (
  `match_id` int(11) NOT NULL,
  `parent_match_id` int(11) NOT NULL,
  `home_team` varchar(50) NOT NULL,
  `away_team` varchar(50) NOT NULL,
  `start_time` datetime NOT NULL,
  `game_id` varchar(6) NOT NULL,
  `competition_id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `instance_id` int(10) NOT NULL DEFAULT 0,
  `bet_closure` datetime NOT NULL,
  `created_by` varchar(60) NOT NULL,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `result` varchar(45) DEFAULT NULL,
  `ht_score` varchar(5) DEFAULT NULL,
  `ft_score` varchar(5) DEFAULT NULL,
  `completed` tinyint(1) DEFAULT 0,
  `priority` int(10) DEFAULT 50,
  `ussd_priority` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bonus_bet`
--

CREATE TABLE `bonus_bet` (
  `bonus_bet_id` int(11) NOT NULL,
  `bet_id` int(11) NOT NULL,
  `bet_amount` decimal(10,2) NOT NULL,
  `possible_win` decimal(10,2) NOT NULL,
  `profile_bonus_id` int(11) DEFAULT NULL,
  `won` tinyint(1) NOT NULL DEFAULT 0,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `ratio` decimal(10,2) NOT NULL,
  `created_by` varchar(70) NOT NULL,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bonus_bet_count`
--

CREATE TABLE `bonus_bet_count` (
  `bonus_bet_count_id` int(11) NOT NULL,
  `profile_bonus_id` int(11) NOT NULL,
  `profile_id` bigint(20) NOT NULL,
  `num_bets` int(5) NOT NULL,
  `created_by` varchar(70) NOT NULL,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bonus_trx`
--

CREATE TABLE `bonus_trx` (
  `id` int(11) NOT NULL,
  `profile_id` bigint(20) NOT NULL,
  `account` varchar(50) NOT NULL,
  `iscredit` smallint(1) NOT NULL,
  `reference` varchar(50) NOT NULL,
  `amount` decimal(10,0) NOT NULL,
  `created_by` varchar(60) NOT NULL,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `profile_bonus_id` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `card_summary`
--

CREATE TABLE `card_summary` (
  `card_summary_id` bigint(20) NOT NULL,
  `parent_match_id` bigint(20) NOT NULL,
  `id` bigint(10) NOT NULL,
  `type` varchar(60) NOT NULL,
  `player` varchar(80) NOT NULL,
  `team` varchar(20) NOT NULL,
  `match_time` int(10) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 0,
  `modified` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `category_id` int(11) NOT NULL,
  `category_name` varchar(120) NOT NULL,
  `status` smallint(1) NOT NULL,
  `sport_id` int(11) NOT NULL,
  `created_by` varchar(70) NOT NULL,
  `betradar_category_id` int(10) NOT NULL DEFAULT 0,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `priority` int(10) DEFAULT 0,
  `max_stake` decimal(10,2) DEFAULT NULL,
  `flag_icon` varchar(200) DEFAULT NULL,
  `country_code` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `competition`
--

CREATE TABLE `competition` (
  `competition_id` int(11) NOT NULL,
  `competition_name` varchar(120) NOT NULL,
  `category` varchar(120) NOT NULL,
  `status` smallint(1) NOT NULL,
  `category_id` int(10) NOT NULL DEFAULT 0,
  `sport_id` int(11) NOT NULL,
  `betradar_competition_id` int(10) NOT NULL DEFAULT 0,
  `created_by` varchar(70) NOT NULL,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `priority` int(10) DEFAULT 0,
  `ussd_priority` int(10) DEFAULT NULL,
  `max_stake` decimal(10,2) DEFAULT NULL,
  `alias` varchar(20) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `delivery_report`
--

CREATE TABLE `delivery_report` (
  `delivery_report_id` bigint(20) NOT NULL,
  `msisdn` varchar(50) NOT NULL,
  `correlator` varchar(70) NOT NULL,
  `no_of_retry` int(10) NOT NULL DEFAULT 0,
  `status` varchar(200) NOT NULL,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `early_bet_white_list`
--

CREATE TABLE `early_bet_white_list` (
  `msisdn` varchar(70) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `event`
--

CREATE TABLE `event` (
  `match_id` int(11) NOT NULL,
  `parent_match_id` int(11) NOT NULL,
  `home_team` varchar(50) NOT NULL,
  `away_team` varchar(50) NOT NULL,
  `start_time` datetime NOT NULL,
  `game_id` varchar(6) NOT NULL,
  `competition_id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `bet_closure` datetime NOT NULL,
  `created_by` varchar(60) NOT NULL,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `result` varchar(45) DEFAULT NULL,
  `ht_score` varchar(5) DEFAULT NULL,
  `ft_score` varchar(5) DEFAULT NULL,
  `completed` tinyint(1) DEFAULT 0,
  `priority` int(10) DEFAULT 50
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `event_odd`
--

CREATE TABLE `event_odd` (
  `event_odd_id` int(11) NOT NULL,
  `parent_match_id` int(11) DEFAULT NULL,
  `sub_type_id` int(11) DEFAULT NULL,
  `max_bet` float(10,2) NOT NULL DEFAULT 10000.00,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `odd_key` varchar(200) NOT NULL,
  `odd_value` varchar(20) DEFAULT NULL,
  `odd_alias` varchar(20) DEFAULT NULL,
  `special_bet_value` varchar(450) DEFAULT NULL,
  `id_of_player` varchar(30) DEFAULT NULL,
  `outcome_id` varchar(60) DEFAULT NULL,
  `special_bet_key` varchar(45) DEFAULT NULL,
  `active` tinyint(4) DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `free_bet`
--

CREATE TABLE `free_bet` (
  `free_bet_id` bigint(20) NOT NULL,
  `profile_id` bigint(20) NOT NULL,
  `no_of_bets` int(11) NOT NULL DEFAULT 1,
  `free_bet_amount` double(10,2) NOT NULL DEFAULT 0.00,
  `status` enum('AWARDED','FAILED','CANCELLED','EXPIRED','INCOMPLETE') NOT NULL DEFAULT 'INCOMPLETE',
  `event_date` date NOT NULL,
  `to_award_on` datetime DEFAULT NULL,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `free_bet_transactions`
--

CREATE TABLE `free_bet_transactions` (
  `free_bet_transactions_id` bigint(20) NOT NULL,
  `bet_id` bigint(20) NOT NULL,
  `profile_id` bigint(20) NOT NULL,
  `free_bet_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ga`
--

CREATE TABLE `ga` (
  `number` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `game`
--

CREATE TABLE `game` (
  `match_id` int(11) NOT NULL,
  `parent_match_id` int(11) NOT NULL,
  `home_team` varchar(50) NOT NULL,
  `away_team` varchar(50) NOT NULL,
  `start_time` datetime NOT NULL,
  `game_id` varchar(6) NOT NULL,
  `competition_id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `bet_closure` datetime NOT NULL,
  `created_by` varchar(60) NOT NULL,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `result` varchar(45) DEFAULT NULL,
  `ht_score` varchar(5) DEFAULT NULL,
  `ft_score` varchar(5) DEFAULT NULL,
  `completed` tinyint(1) DEFAULT 0,
  `priority` int(10) DEFAULT 50
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `game_ids`
--

CREATE TABLE `game_ids` (
  `id` bigint(20) NOT NULL,
  `number` int(20) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 0,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `game_request`
--

CREATE TABLE `game_request` (
  `request_id` int(11) NOT NULL,
  `match_id` int(11) NOT NULL,
  `profile_id` int(11) NOT NULL,
  `offset` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `gk_bonus_campaign`
--

CREATE TABLE `gk_bonus_campaign` (
  `id` int(11) NOT NULL,
  `cdate` timestamp NOT NULL DEFAULT current_timestamp(),
  `clabel` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `start_date` datetime DEFAULT NULL,
  `mcount` int(11) NOT NULL DEFAULT 0,
  `amount` double NOT NULL DEFAULT 0,
  `message` varchar(160) COLLATE latin1_general_ci DEFAULT NULL,
  `process` varchar(100) COLLATE latin1_general_ci NOT NULL DEFAULT 'PENDING',
  `cstatus` varchar(100) COLLATE latin1_general_ci NOT NULL DEFAULT 'ACTIVE',
  `expiry` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `gk_bonus_upload`
--

CREATE TABLE `gk_bonus_upload` (
  `id` int(11) NOT NULL,
  `udate` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `campaign_id` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `upload_label` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `msisdn` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `amount` double NOT NULL,
  `cstatus` varchar(255) COLLATE latin1_general_ci NOT NULL DEFAULT 'PENDING',
  `claim` varchar(255) COLLATE latin1_general_ci NOT NULL DEFAULT 'PENDING'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `gk_sms`
--

CREATE TABLE `gk_sms` (
  `id` int(11) NOT NULL,
  `sdate` timestamp NOT NULL DEFAULT current_timestamp(),
  `msisdn` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `message` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `response` text COLLATE latin1_general_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `gk_users`
--

CREATE TABLE `gk_users` (
  `uid` int(11) NOT NULL,
  `username` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `userpass` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `fullname` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `module` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `lastip` varchar(255) COLLATE latin1_general_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `gr_profile_player`
--

CREATE TABLE `gr_profile_player` (
  `player_id` bigint(20) NOT NULL,
  `profile_id` bigint(20) NOT NULL,
  `player_name` varchar(70) DEFAULT NULL,
  `gr_player_id` bigint(100) NOT NULL,
  `created_by` varchar(100) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `gr_tickets`
--

CREATE TABLE `gr_tickets` (
  `gr_ticket_id` bigint(20) NOT NULL,
  `gr_ticket` bigint(20) NOT NULL DEFAULT 0,
  `profile_id` bigint(20) NOT NULL,
  `gr_ticket_type` varchar(70) NOT NULL DEFAULT '',
  `bet_amount` decimal(10,2) NOT NULL,
  `possible_win` decimal(10,2) NOT NULL DEFAULT 0.00,
  `status` smallint(1) NOT NULL,
  `created_by` varchar(70) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `inactive2_subs`
--

CREATE TABLE `inactive2_subs` (
  `id` int(11) NOT NULL,
  `msisdn` varchar(50) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `inactive_nums`
--

CREATE TABLE `inactive_nums` (
  `msisdn` varchar(70) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `inactive_profile`
--

CREATE TABLE `inactive_profile` (
  `id` bigint(20) NOT NULL,
  `profile_id` varchar(45) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `inbox`
--

CREATE TABLE `inbox` (
  `inbox_id` int(11) NOT NULL,
  `network` varchar(50) DEFAULT NULL,
  `shortcode` int(5) DEFAULT NULL,
  `msisdn` varchar(20) DEFAULT NULL,
  `message` varchar(300) DEFAULT NULL,
  `linkid` varchar(100) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `created_by` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jackpot_bet`
--

CREATE TABLE `jackpot_bet` (
  `jackpot_bet_id` bigint(20) NOT NULL,
  `bet_id` int(11) NOT NULL,
  `jackpot_event_id` bigint(20) NOT NULL,
  `status` enum('ACTIVE','CANCELLED','FINISHED') NOT NULL DEFAULT 'ACTIVE',
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jackpot_event`
--

CREATE TABLE `jackpot_event` (
  `jackpot_event_id` bigint(20) NOT NULL,
  `jackpot_type` bigint(20) NOT NULL,
  `jackpot_name` varchar(250) NOT NULL,
  `created_by` varchar(70) NOT NULL,
  `status` enum('CANCELLED','ACTIVE','INACTIVE','SUSPENDED','FINISHED','OTHER') DEFAULT 'INACTIVE',
  `bet_amount` decimal(10,2) NOT NULL,
  `total_games` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `jackpot_amount` decimal(10,2) DEFAULT NULL,
  `jp_key` varchar(10) NOT NULL DEFAULT 'jp',
  `requisite_wins` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jackpot_match`
--

CREATE TABLE `jackpot_match` (
  `jackpot_match_id` bigint(20) NOT NULL,
  `parent_match_id` bigint(20) NOT NULL,
  `status` enum('CANCELLED','POSTPONED','ACTIVE','INACTIVE') NOT NULL DEFAULT 'ACTIVE',
  `created_by` varchar(70) NOT NULL,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `jackpot_event_id` int(20) NOT NULL,
  `game_order` int(11) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jackpot_trx`
--

CREATE TABLE `jackpot_trx` (
  `jackpot_trx_id` bigint(20) NOT NULL,
  `trx_id` int(11) NOT NULL,
  `jackpot_event_id` bigint(20) NOT NULL,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jackpot_type`
--

CREATE TABLE `jackpot_type` (
  `jackpot_type_id` bigint(20) NOT NULL,
  `name` varchar(250) NOT NULL,
  `sub_type_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jackpot_winner`
--

CREATE TABLE `jackpot_winner` (
  `jackpot_winner_id` bigint(20) NOT NULL,
  `win_amount` decimal(10,2) NOT NULL,
  `bet_id` int(11) NOT NULL,
  `jackpot_event_id` bigint(20) NOT NULL,
  `msisdn` varchar(45) NOT NULL,
  `total_games_correct` int(11) NOT NULL,
  `created_by` varchar(70) NOT NULL,
  `status` int(11) DEFAULT 1,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jpbonus_award`
--

CREATE TABLE `jpbonus_award` (
  `id` int(11) NOT NULL,
  `jackpot_event_id` int(11) NOT NULL,
  `total_games_correct` int(11) NOT NULL,
  `jackpot_bonus` decimal(10,2) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 0,
  `shikabet_points_bonus` decimal(10,2) NOT NULL,
  `created_by` varchar(255) NOT NULL,
  `approved_by` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jp_bet_cancel_data`
--

CREATE TABLE `jp_bet_cancel_data` (
  `profile_id` int(11) NOT NULL,
  `bet_id` bigint(20) NOT NULL,
  `id` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `latest_alive`
--

CREATE TABLE `latest_alive` (
  `alive_id` bigint(20) NOT NULL,
  `alive_string` text DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `old_alive_string` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `live_betting_control`
--

CREATE TABLE `live_betting_control` (
  `id` int(11) NOT NULL,
  `disabled` tinyint(1) NOT NULL DEFAULT 0,
  `created` timestamp NOT NULL DEFAULT current_timestamp(),
  `event` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `live_enabled_market`
--

CREATE TABLE `live_enabled_market` (
  `id` int(11) NOT NULL,
  `sub_type_id` int(10) NOT NULL DEFAULT 0,
  `created` timestamp NOT NULL DEFAULT current_timestamp(),
  `status` tinyint(1) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `live_match`
--

CREATE TABLE `live_match` (
  `match_id` int(11) NOT NULL,
  `parent_match_id` int(11) NOT NULL,
  `home_team` varchar(50) NOT NULL,
  `away_team` varchar(50) NOT NULL,
  `start_time` datetime NOT NULL,
  `game_id` varchar(45) NOT NULL,
  `competition_id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `instance_id` int(10) NOT NULL DEFAULT 0,
  `bet_closure` datetime NOT NULL,
  `created_by` varchar(60) NOT NULL,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `result` varchar(45) DEFAULT NULL,
  `ht_score` varchar(5) DEFAULT NULL,
  `ft_score` varchar(5) DEFAULT NULL,
  `completed` tinyint(1) DEFAULT 0,
  `priority` int(10) DEFAULT 50,
  `match_time` varchar(10) DEFAULT NULL,
  `score` varchar(60) DEFAULT NULL,
  `event_status` varchar(60) DEFAULT NULL,
  `bet_status` varchar(60) DEFAULT NULL,
  `match_status` varchar(20) DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `home_red_card` tinyint(2) DEFAULT NULL,
  `away_red_card` tinyint(2) DEFAULT NULL,
  `home_yellow_card` tinyint(2) DEFAULT NULL,
  `away_yellow_card` tinyint(2) DEFAULT NULL,
  `home_corner` tinyint(4) DEFAULT NULL,
  `away_corner` tinyint(4) DEFAULT NULL,
  `betradar_timestamp` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `live_meta_history`
--

CREATE TABLE `live_meta_history` (
  `live_odds_meta_id` bigint(20) NOT NULL,
  `parent_match_id` bigint(20) NOT NULL,
  `match_time` tinytext NOT NULL,
  `score` tinytext NOT NULL,
  `bet_status` tinytext NOT NULL,
  `match_status` varchar(20) DEFAULT NULL,
  `event_status` varchar(60) DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 0,
  `sequence_no` bigint(20) NOT NULL DEFAULT 0,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `betradar_timestamp` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `live_odds`
--

CREATE TABLE `live_odds` (
  `live_odds_change_id` bigint(20) NOT NULL,
  `parent_match_id` bigint(20) NOT NULL,
  `sub_type_id` int(10) NOT NULL,
  `odd_key` varchar(100) NOT NULL,
  `odd_value` varchar(100) NOT NULL,
  `special_bet_value` varchar(60) DEFAULT NULL,
  `match_time` tinytext DEFAULT NULL,
  `score` tinytext NOT NULL,
  `bet_status` tinytext DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 0,
  `market_active` varchar(20) DEFAULT NULL,
  `odd_active` tinyint(1) NOT NULL DEFAULT 0,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `sequence_no` bigint(20) DEFAULT NULL,
  `betradar_timestamp` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `live_odds_change`
--

CREATE TABLE `live_odds_change` (
  `live_odds_change_id` bigint(20) NOT NULL,
  `parent_match_id` bigint(20) NOT NULL,
  `sub_type_id` int(10) NOT NULL,
  `odd_key` varchar(100) NOT NULL,
  `odd_value` varchar(20) NOT NULL,
  `special_bet_value` varchar(60) DEFAULT NULL,
  `match_time` tinytext DEFAULT NULL,
  `score` tinytext DEFAULT NULL,
  `bet_status` tinytext DEFAULT NULL,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `sequence_no` bigint(20) DEFAULT NULL,
  `betradar_timestamp` timestamp NULL DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `market_active` varchar(45) DEFAULT NULL,
  `odd_active` tinyint(1) NOT NULL DEFAULT 0,
  `market_name` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `live_odds_meta`
--

CREATE TABLE `live_odds_meta` (
  `live_odds_meta_id` bigint(20) NOT NULL,
  `parent_match_id` bigint(20) NOT NULL,
  `match_time` varchar(10) DEFAULT NULL,
  `score` varchar(60) DEFAULT NULL,
  `bet_status` varchar(60) DEFAULT NULL,
  `match_status` varchar(20) DEFAULT NULL,
  `event_status` varchar(60) DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 0,
  `sequence_no` bigint(20) NOT NULL DEFAULT 0,
  `home_red_card` tinyint(1) NOT NULL DEFAULT 0,
  `away_red_card` tinyint(1) NOT NULL DEFAULT 0,
  `home_yellow_card` int(10) DEFAULT NULL,
  `away_yellow_card` int(10) DEFAULT NULL,
  `home_corner` int(10) DEFAULT NULL,
  `away_corner` int(10) DEFAULT NULL,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `betradar_timestamp` datetime NOT NULL,
  `set_score` varchar(60) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `logbetcancel`
--

CREATE TABLE `logbetcancel` (
  `id` int(11) NOT NULL,
  `parent_match_id` int(20) DEFAULT NULL,
  `sub_type_id` int(11) DEFAULT NULL,
  `special_bet_value` varchar(45) DEFAULT NULL,
  `void_reason` varchar(100) DEFAULT NULL,
  `created` datetime DEFAULT current_timestamp(),
  `modified` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `created_by` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ls_betmatch`
--

CREATE TABLE `ls_betmatch` (
  `id` bigint(20) NOT NULL,
  `betradar_match_id` int(11) DEFAULT NULL,
  `betradar_tournament_id` int(11) DEFAULT NULL,
  `created` date DEFAULT NULL,
  `match_name` varchar(255) DEFAULT NULL,
  `matchdate` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `parent_match_id` int(11) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `status_code` varchar(255) DEFAULT NULL,
  `winner` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ls_card`
--

CREATE TABLE `ls_card` (
  `id` bigint(20) NOT NULL,
  `card_id` int(11) DEFAULT NULL,
  `created` date DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `parent_match_id` int(11) DEFAULT NULL,
  `player_id` int(11) DEFAULT NULL,
  `player_name` varchar(255) DEFAULT NULL,
  `player_team` varchar(255) DEFAULT NULL,
  `time` int(11) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ls_category`
--

CREATE TABLE `ls_category` (
  `id` bigint(20) NOT NULL,
  `betradar_category_id` int(11) DEFAULT NULL,
  `betradar_sport_id` int(11) DEFAULT NULL,
  `category_name` varchar(255) DEFAULT NULL,
  `created` date DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ls_city`
--

CREATE TABLE `ls_city` (
  `id` bigint(20) NOT NULL,
  `city_id` int(11) DEFAULT NULL,
  `city_name` varchar(255) DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `created` date DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ls_country`
--

CREATE TABLE `ls_country` (
  `id` bigint(20) NOT NULL,
  `country_id` int(11) DEFAULT NULL,
  `country_name` varchar(255) DEFAULT NULL,
  `created` date DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ls_goal`
--

CREATE TABLE `ls_goal` (
  `id` bigint(20) NOT NULL,
  `created` date DEFAULT NULL,
  `goal_id` int(11) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `parent_match_id` int(11) DEFAULT NULL,
  `player_id` int(11) DEFAULT NULL,
  `player_name` varchar(255) DEFAULT NULL,
  `scoringteam` varchar(255) DEFAULT NULL,
  `team1goal` varchar(255) DEFAULT NULL,
  `team2goal` varchar(255) DEFAULT NULL,
  `time` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ls_line_up`
--

CREATE TABLE `ls_line_up` (
  `id` bigint(20) NOT NULL,
  `created` date DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `team_player_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ls_livescore_data`
--

CREATE TABLE `ls_livescore_data` (
  `id` bigint(20) NOT NULL,
  `created` date DEFAULT NULL,
  `generatedat` datetime DEFAULT NULL,
  `generatedfor` varchar(255) DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ls_match_detail`
--

CREATE TABLE `ls_match_detail` (
  `id` bigint(20) NOT NULL,
  `created` date DEFAULT NULL,
  `current_period_start` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `parent_match_id` int(11) DEFAULT NULL,
  `time` datetime DEFAULT NULL,
  `winning_team` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ls_player`
--

CREATE TABLE `ls_player` (
  `id` bigint(20) NOT NULL,
  `created` date DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `parent_match_id` int(11) DEFAULT NULL,
  `player_id` int(11) DEFAULT NULL,
  `player_name` varchar(255) DEFAULT NULL,
  `player_team` varchar(255) DEFAULT NULL,
  `shirt_number` int(11) DEFAULT NULL,
  `substitute` varchar(255) DEFAULT NULL,
  `team_player_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ls_referee`
--

CREATE TABLE `ls_referee` (
  `id` bigint(20) NOT NULL,
  `country_id` int(11) DEFAULT NULL,
  `country_name` varchar(255) DEFAULT NULL,
  `created` date DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `parent_match_id` int(11) DEFAULT NULL,
  `referee_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ls_scores`
--

CREATE TABLE `ls_scores` (
  `id` bigint(20) NOT NULL,
  `created` date DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `parent_match_id` int(11) DEFAULT NULL,
  `scoretype` int(11) DEFAULT NULL,
  `team_1_score` varchar(255) DEFAULT NULL,
  `team_2_score` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ls_sport`
--

CREATE TABLE `ls_sport` (
  `id` bigint(20) NOT NULL,
  `betradar_sport_id` int(11) DEFAULT NULL,
  `created` date DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `sport_name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ls_substitution`
--

CREATE TABLE `ls_substitution` (
  `id` bigint(20) NOT NULL,
  `created` date DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `parent_match_id` int(11) DEFAULT NULL,
  `player_in_id` int(11) DEFAULT NULL,
  `player_in_name` varchar(255) DEFAULT NULL,
  `player_out_id` int(11) DEFAULT NULL,
  `player_out_name` varchar(255) DEFAULT NULL,
  `player_team` varchar(255) DEFAULT NULL,
  `substituition_id` int(11) DEFAULT NULL,
  `time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ls_team`
--

CREATE TABLE `ls_team` (
  `id` bigint(20) NOT NULL,
  `betradar_team_id` int(11) DEFAULT NULL,
  `created` date DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `parent_match_id` int(11) DEFAULT NULL,
  `t` int(11) DEFAULT NULL,
  `team_name` varchar(255) DEFAULT NULL,
  `unique_team_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ls_tournament`
--

CREATE TABLE `ls_tournament` (
  `id` bigint(20) NOT NULL,
  `betradar_category_id` int(11) DEFAULT NULL,
  `betradar_tournament_id` int(11) DEFAULT NULL,
  `created` date DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `tournament_name` varchar(255) DEFAULT NULL,
  `unique_tournament_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ls_venue`
--

CREATE TABLE `ls_venue` (
  `id` bigint(20) NOT NULL,
  `country_id` int(11) DEFAULT NULL,
  `created` date DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `parent_match_id` int(11) DEFAULT NULL,
  `stadium` varchar(255) DEFAULT NULL,
  `stadium_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `market_priority`
--

CREATE TABLE `market_priority` (
  `id` int(11) NOT NULL,
  `sub_type_id` int(10) NOT NULL DEFAULT 0,
  `priority` int(2) DEFAULT 0,
  `created` timestamp NOT NULL DEFAULT current_timestamp(),
  `status` tinyint(1) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `match`
--

CREATE TABLE `match` (
  `match_id` int(11) NOT NULL,
  `parent_match_id` int(11) NOT NULL,
  `home_team` varchar(50) NOT NULL,
  `away_team` varchar(50) NOT NULL,
  `start_time` datetime NOT NULL,
  `game_id` varchar(45) NOT NULL,
  `competition_id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `instance_id` int(10) NOT NULL DEFAULT 0,
  `bet_closure` datetime NOT NULL,
  `created_by` varchar(60) NOT NULL,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `result` varchar(45) DEFAULT NULL,
  `ht_score` varchar(5) DEFAULT NULL,
  `ft_score` varchar(5) DEFAULT NULL,
  `completed` tinyint(1) DEFAULT 0,
  `priority` int(10) DEFAULT 50,
  `ussd_priority` int(10) DEFAULT NULL,
  `peer_priority` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `matchid`
--

CREATE TABLE `matchid` (
  `id` int(11) NOT NULL,
  `match_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `parent` int(11) DEFAULT NULL,
  `route` varchar(255) DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  `data` blob DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `migration`
--

CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `missed_withdrawals`
--

CREATE TABLE `missed_withdrawals` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `withdrawal_id` bigint(20) UNSIGNED DEFAULT NULL,
  `message` longtext NOT NULL,
  `descr` longtext DEFAULT NULL,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `mobile_app_profile`
--

CREATE TABLE `mobile_app_profile` (
  `id` int(10) UNSIGNED NOT NULL,
  `profile_id` bigint(20) UNSIGNED NOT NULL,
  `msisdn` varchar(30) NOT NULL,
  `device_id` varchar(200) NOT NULL,
  `token_id` varchar(200) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `app` varchar(120) DEFAULT 'Shikabet',
  `date_created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mobile_app_version`
--

CREATE TABLE `mobile_app_version` (
  `id` int(11) NOT NULL,
  `current_version` varchar(20) NOT NULL,
  `app_name` varchar(120) DEFAULT 'Shikabet',
  `status` int(11) NOT NULL DEFAULT 1,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mpesa_rate`
--

CREATE TABLE `mpesa_rate` (
  `id` bigint(20) NOT NULL,
  `min_amount` float NOT NULL,
  `max_amount` float NOT NULL,
  `charge` float NOT NULL,
  `created` datetime DEFAULT NULL,
  `updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `mpesa_transaction`
--

CREATE TABLE `mpesa_transaction` (
  `mpesa_transaction_id` bigint(20) NOT NULL,
  `msisdn` bigint(20) NOT NULL,
  `transaction_time` datetime NOT NULL,
  `message` varchar(300) NOT NULL,
  `mpesa_customer_id` varchar(50) NOT NULL,
  `account_no` varchar(100) NOT NULL,
  `mpesa_code` varchar(100) NOT NULL,
  `mpesa_amt` decimal(53,2) NOT NULL,
  `mpesa_sender` varchar(100) NOT NULL,
  `business_number` varchar(45) DEFAULT NULL,
  `enc_params` varchar(250) DEFAULT NULL,
  `promo_code` varchar(50) DEFAULT NULL,
  `paid_by` varchar(45) DEFAULT NULL,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `msi`
--

CREATE TABLE `msi` (
  `number` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `msisdns_a`
--

CREATE TABLE `msisdns_a` (
  `msisdn` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mts_exception`
--

CREATE TABLE `mts_exception` (
  `mts_exception_id` bigint(20) NOT NULL,
  `name` varchar(200) NOT NULL,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `mts_ticket_submit`
--

CREATE TABLE `mts_ticket_submit` (
  `mts_ticket_submit_id` bigint(20) NOT NULL,
  `bet_id` bigint(20) NOT NULL,
  `mts_ticket` varchar(200) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1,
  `response` varchar(200) DEFAULT NULL,
  `message` text CHARACTER SET latin1 DEFAULT NULL,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mts_validation_code`
--

CREATE TABLE `mts_validation_code` (
  `mts_validation_code_id` bigint(20) NOT NULL,
  `scenario` text NOT NULL,
  `outcome` varchar(200) NOT NULL,
  `code` varchar(100) NOT NULL,
  `message` text DEFAULT NULL,
  `mts_exception_id` bigint(20) DEFAULT NULL,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `network_charge_range`
--

CREATE TABLE `network_charge_range` (
  `network_charge_range_id` int(13) NOT NULL,
  `network` varchar(10) NOT NULL DEFAULT 'Network',
  `minimum` decimal(10,2) NOT NULL DEFAULT 0.00,
  `maximum` varchar(45) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `nums_pro`
--

CREATE TABLE `nums_pro` (
  `pro_id` bigint(20) NOT NULL,
  `profile_id` int(10) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `odds_history`
--

CREATE TABLE `odds_history` (
  `odd_history_id` int(11) NOT NULL,
  `parent_match_id` int(11) DEFAULT NULL,
  `sub_type_id` int(11) DEFAULT NULL,
  `special_bet_value` varchar(60) DEFAULT NULL,
  `odd_key` varchar(100) NOT NULL,
  `odd_value` varchar(100) NOT NULL,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `odds_subtype`
--

CREATE TABLE `odds_subtype` (
  `odds_subtype_id` bigint(20) NOT NULL,
  `sub_type_id` int(10) NOT NULL DEFAULT 0,
  `freetext` tinytext NOT NULL,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `special_bet_value` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `odd_history`
--

CREATE TABLE `odd_history` (
  `odd_history_id` int(11) NOT NULL,
  `parent_match_id` int(11) DEFAULT NULL,
  `sub_type_id` int(11) DEFAULT NULL,
  `odd_key` varchar(20) NOT NULL,
  `odd_value` varchar(20) DEFAULT NULL,
  `special_bet_value` varchar(100) DEFAULT NULL,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `active` tinyint(4) DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `odd_key_alias`
--

CREATE TABLE `odd_key_alias` (
  `odd_key_alias_id` int(10) NOT NULL,
  `sub_type_id` int(20) NOT NULL,
  `odd_key` varchar(45) DEFAULT NULL,
  `odd_key_alias` varchar(10) NOT NULL,
  `special_bet_value` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `odd_type`
--

CREATE TABLE `odd_type` (
  `bet_type_id` int(11) NOT NULL,
  `name` varchar(70) NOT NULL,
  `created_by` varchar(70) NOT NULL,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `sub_type_id` int(11) NOT NULL,
  `parent_match_id` bigint(20) DEFAULT NULL,
  `live_bet` tinyint(4) DEFAULT 0,
  `short_name` varchar(10) NOT NULL,
  `priority` decimal(10,2) NOT NULL DEFAULT 0.00,
  `special_bet_value` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `odd_type_group`
--

CREATE TABLE `odd_type_group` (
  `id` int(11) NOT NULL,
  `sub_type_id` int(11) NOT NULL,
  `group_name` varchar(45) NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp(),
  `special_bet_value` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `odd_type_old`
--

CREATE TABLE `odd_type_old` (
  `bet_type_id` int(11) NOT NULL,
  `name` varchar(70) NOT NULL,
  `created_by` varchar(70) NOT NULL,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `sub_type_id` int(11) NOT NULL,
  `parent_match_id` bigint(20) DEFAULT NULL,
  `live_bet` tinyint(4) DEFAULT 0,
  `short_name` varchar(10) NOT NULL,
  `priority` decimal(10,2) NOT NULL DEFAULT 0.00,
  `special_bet_value` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `outbox`
--

CREATE TABLE `outbox` (
  `outbox_id` bigint(20) NOT NULL,
  `shortcode` varchar(30) DEFAULT NULL,
  `network` varchar(50) DEFAULT NULL,
  `profile_id` bigint(20) DEFAULT NULL,
  `linkid` varchar(100) DEFAULT NULL,
  `sdp_status` varchar(100) DEFAULT NULL,
  `reference` varchar(100) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_sent` datetime DEFAULT NULL,
  `retry_status` tinyint(1) DEFAULT 0,
  `modified` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `text` text DEFAULT NULL,
  `msisdn` varchar(25) DEFAULT NULL,
  `sdp_id` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `outcome`
--

CREATE TABLE `outcome` (
  `match_result_id` int(11) NOT NULL,
  `sub_type_id` int(20) NOT NULL,
  `parent_match_id` int(11) NOT NULL,
  `special_bet_value` varchar(20) NOT NULL DEFAULT '',
  `live_bet` tinyint(4) DEFAULT 0,
  `created_by` varchar(70) NOT NULL,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT current_timestamp(),
  `status` int(1) DEFAULT 0,
  `approved_by` varchar(70) DEFAULT NULL,
  `approved_status` tinyint(4) DEFAULT 0,
  `date_approved` datetime DEFAULT NULL,
  `winning_outcome` varchar(200) NOT NULL,
  `is_winning_outcome` tinyint(1) DEFAULT NULL,
  `void_factor` float DEFAULT NULL,
  `outcome_id` varchar(60) DEFAULT NULL,
  `special_bet_key` varchar(60) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `outcome_backup`
--

CREATE TABLE `outcome_backup` (
  `match_result_id` int(11) NOT NULL,
  `sub_type_id` int(20) NOT NULL,
  `parent_match_id` int(11) NOT NULL,
  `special_bet_value` varchar(20) NOT NULL DEFAULT '',
  `live_bet` tinyint(4) DEFAULT 0,
  `created_by` varchar(70) NOT NULL,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT current_timestamp(),
  `status` int(1) DEFAULT 0,
  `approved_by` varchar(70) DEFAULT NULL,
  `approved_status` tinyint(4) DEFAULT 0,
  `date_approved` datetime DEFAULT NULL,
  `winning_outcome` varchar(200) NOT NULL,
  `is_winning_outcome` tinyint(1) DEFAULT NULL,
  `void_factor` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `outcome_o`
--

CREATE TABLE `outcome_o` (
  `match_result_id` int(11) NOT NULL,
  `sub_type_id` int(20) NOT NULL,
  `parent_match_id` int(11) NOT NULL,
  `special_bet_value` varchar(20) DEFAULT NULL,
  `created_by` varchar(70) NOT NULL,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT current_timestamp(),
  `status` int(1) DEFAULT 0,
  `winning_outcome` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `outcome_old`
--

CREATE TABLE `outcome_old` (
  `match_result_id` int(11) NOT NULL,
  `sub_type_id` int(20) NOT NULL,
  `parent_match_id` int(11) NOT NULL,
  `created_by` varchar(70) NOT NULL,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT current_timestamp(),
  `status` int(1) DEFAULT 0,
  `winning_outcome` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `outcome_test`
--

CREATE TABLE `outcome_test` (
  `match_result_id` int(11) NOT NULL,
  `sub_type_id` int(20) NOT NULL,
  `parent_match_id` int(11) NOT NULL,
  `special_bet_value` varchar(20) NOT NULL DEFAULT '',
  `live_bet` tinyint(4) DEFAULT 0,
  `created_by` varchar(70) NOT NULL,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT current_timestamp(),
  `status` int(1) DEFAULT 0,
  `approved_by` varchar(70) DEFAULT NULL,
  `approved_status` tinyint(4) DEFAULT 0,
  `date_approved` datetime DEFAULT NULL,
  `winning_outcome` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `outright`
--

CREATE TABLE `outright` (
  `outright_id` int(11) NOT NULL,
  `parent_outright_id` int(11) NOT NULL,
  `event_name` varchar(100) NOT NULL,
  `event_date` datetime NOT NULL,
  `event_end_date` datetime NOT NULL,
  `game_id` varchar(20) DEFAULT NULL,
  `competition_id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `instance_id` int(10) NOT NULL DEFAULT 0,
  `created_by` varchar(60) NOT NULL,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `result` varchar(45) DEFAULT NULL,
  `completed` tinyint(1) DEFAULT 0,
  `priority` int(10) DEFAULT 50
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `outright_competitor`
--

CREATE TABLE `outright_competitor` (
  `competitor_id` int(11) NOT NULL,
  `parent_outright_id` int(11) NOT NULL,
  `betradar_competitor_id` int(11) NOT NULL,
  `betradar_super_id` int(11) NOT NULL,
  `competitor_name` varchar(100) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `priority` int(10) DEFAULT 50,
  `created_by` varchar(60) NOT NULL,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `outright_odd`
--

CREATE TABLE `outright_odd` (
  `odd_id` int(11) NOT NULL,
  `parent_outright_id` int(11) NOT NULL,
  `betradar_competitor_id` int(11) NOT NULL,
  `odd_type` varchar(20) NOT NULL,
  `odd_value` varchar(20) DEFAULT NULL,
  `special_bet_value` varchar(10) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_by` varchar(60) NOT NULL,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `outright_odd_history`
--

CREATE TABLE `outright_odd_history` (
  `odd_id` int(11) NOT NULL,
  `parent_outright_id` int(11) NOT NULL,
  `betradar_competitor_id` int(11) NOT NULL,
  `odd_type` varchar(20) NOT NULL,
  `odd_value` int(10) NOT NULL,
  `special_bet_value` varchar(10) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_by` varchar(60) NOT NULL,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `outright_outcome`
--

CREATE TABLE `outright_outcome` (
  `outcome_id` int(11) NOT NULL,
  `parent_outright_id` int(11) NOT NULL,
  `betradar_competitor_id` int(11) NOT NULL,
  `odd_type` varchar(20) DEFAULT NULL,
  `special_bet_value` varchar(10) NOT NULL DEFAULT '',
  `outcome` varchar(60) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_by` varchar(60) NOT NULL,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `paybill_tariff`
--

CREATE TABLE `paybill_tariff` (
  `tariff_id` int(13) NOT NULL,
  `tarriff_name` varchar(45) NOT NULL DEFAULT 'Tariff',
  `customer_charge` decimal(10,2) NOT NULL DEFAULT 0.00,
  `business_charge` decimal(10,2) NOT NULL DEFAULT 0.00,
  `total` decimal(10,2) NOT NULL DEFAULT 0.00,
  `network_range_id` int(13) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `peer_bet`
--

CREATE TABLE `peer_bet` (
  `peer_bet_id` int(11) NOT NULL,
  `bet_id` int(11) DEFAULT NULL,
  `beshte_id` int(11) DEFAULT NULL,
  `profile_id` int(20) NOT NULL,
  `parent_match_id` int(20) NOT NULL,
  `sub_type_id` int(20) NOT NULL,
  `pick` varchar(100) DEFAULT NULL,
  `bet_amount` float(10,2) DEFAULT NULL,
  `commission` float(10,2) DEFAULT NULL,
  `peer_msisdn` varchar(25) DEFAULT NULL,
  `status` enum('PENDING','COMPLETE','RESULTED') DEFAULT NULL,
  `created_by` varchar(70) NOT NULL,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `playground`
--

CREATE TABLE `playground` (
  `profile_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `prematch_disabled_market`
--

CREATE TABLE `prematch_disabled_market` (
  `id` int(11) NOT NULL,
  `sub_type_id` int(10) NOT NULL DEFAULT 0,
  `created` timestamp NOT NULL DEFAULT current_timestamp(),
  `status` tinyint(1) DEFAULT 0,
  `special_bet_value` varchar(200) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `profile`
--

CREATE TABLE `profile` (
  `profile_id` bigint(20) NOT NULL,
  `msisdn` varchar(45) NOT NULL DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `status` smallint(1) DEFAULT NULL,
  `modified` timestamp NULL DEFAULT NULL,
  `created_by` varchar(45) DEFAULT NULL,
  `network` varchar(50) DEFAULT NULL,
  `message_count` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `profile_balance`
--

CREATE TABLE `profile_balance` (
  `profile_balance_id` int(10) NOT NULL,
  `profile_id` bigint(20) NOT NULL,
  `balance` decimal(10,2) NOT NULL,
  `transaction_id` bigint(20) NOT NULL,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT current_timestamp(),
  `bonus_balance` decimal(10,2) DEFAULT 0.00
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `profile_bonus`
--

CREATE TABLE `profile_bonus` (
  `profile_bonus_id` int(11) NOT NULL,
  `profile_id` bigint(20) NOT NULL,
  `referred_msisdn` bigint(20) NOT NULL,
  `bonus_amount` decimal(10,2) NOT NULL DEFAULT 0.00,
  `status` enum('NEW','CLAIMED','EXPIRED','CANCELLED','USED') DEFAULT NULL,
  `expiry_date` datetime NOT NULL,
  `date_created` datetime NOT NULL,
  `updated` timestamp NOT NULL DEFAULT current_timestamp(),
  `bet_on_status` smallint(1) DEFAULT NULL,
  `created_by` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `profile_setting`
--

CREATE TABLE `profile_setting` (
  `profile_setting_id` int(11) NOT NULL,
  `profile_id` int(20) NOT NULL,
  `balance` decimal(10,2) NOT NULL,
  `status` int(1) NOT NULL,
  `verification_code` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `reference_id` varchar(20) DEFAULT NULL,
  `created` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `password` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `profile_settings`
--

CREATE TABLE `profile_settings` (
  `profile_setting_id` bigint(20) NOT NULL,
  `profile_id` bigint(20) DEFAULT NULL,
  `balance` bigint(20) DEFAULT 0,
  `status` smallint(1) DEFAULT 0,
  `verification_code` varchar(10) DEFAULT NULL,
  `name` varchar(250) DEFAULT NULL,
  `reference_id` varchar(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `password` text DEFAULT NULL,
  `max_stake` decimal(10,2) DEFAULT 0.00,
  `single_bet_max_stake` decimal(10,2) DEFAULT 0.00,
  `multibet_bet_max_stake` decimal(10,2) DEFAULT 0.00,
  `max_daily_possible_win` decimal(10,2) DEFAULT 0.00
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `reconciliation`
--

CREATE TABLE `reconciliation` (
  `mpesa_ref` varchar(20) NOT NULL,
  `date` datetime NOT NULL,
  `narration` varchar(60) NOT NULL,
  `status` varchar(20) NOT NULL,
  `amount_out` decimal(10,0) NOT NULL,
  `amount_in` decimal(10,0) NOT NULL,
  `balance` decimal(10,2) DEFAULT NULL,
  `balance_confirmed` varchar(20) DEFAULT NULL,
  `transaction_type` varchar(20) DEFAULT NULL,
  `other_party` varchar(60) DEFAULT NULL,
  `transaction_party` varchar(60) DEFAULT NULL,
  `transaction_id` varchar(60) DEFAULT NULL,
  `remarks` varchar(60) DEFAULT NULL,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `recon_references`
--

CREATE TABLE `recon_references` (
  `mpesa_ref` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `running_balance`
--

CREATE TABLE `running_balance` (
  `id` int(11) NOT NULL,
  `profile_id` bigint(20) NOT NULL,
  `account` varchar(50) NOT NULL,
  `amount` decimal(10,0) NOT NULL,
  `running_balance` decimal(10,0) NOT NULL,
  `created_by` varchar(60) NOT NULL,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `scorepesa_point`
--

CREATE TABLE `scorepesa_point` (
  `shikabet_point_id` bigint(20) NOT NULL,
  `profile_id` bigint(20) NOT NULL,
  `points` decimal(20,2) UNSIGNED NOT NULL,
  `redeemed_amount` decimal(20,2) NOT NULL,
  `created_by` varchar(200) NOT NULL,
  `status` enum('ACTIVE','INACTIVE','SUSPENDED') NOT NULL,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `scorepesa_point_bet`
--

CREATE TABLE `scorepesa_point_bet` (
  `shikabet_point_bet_id` bigint(20) NOT NULL,
  `bet_id` bigint(20) NOT NULL,
  `shikabet_point_trx_id` bigint(20) DEFAULT NULL,
  `points` double(10,2) NOT NULL,
  `amount` decimal(20,2) NOT NULL,
  `created_by` varchar(200) NOT NULL,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `scorepesa_point_summary`
--

CREATE TABLE `scorepesa_point_summary` (
  `msisdn` varchar(30) NOT NULL,
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `scorepesa_point_trx`
--

CREATE TABLE `scorepesa_point_trx` (
  `shikabet_point_trx_id` bigint(20) NOT NULL,
  `trx_id` bigint(20) NOT NULL,
  `points` decimal(10,2) NOT NULL,
  `trx_type` enum('CREDIT','DEBIT') NOT NULL,
  `status` enum('REDEEM','GAIN','TRANSFER','CANCELLED') NOT NULL,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `scorepesa_profile_names`
--

CREATE TABLE `scorepesa_profile_names` (
  `msisdn` varchar(30) NOT NULL,
  `names` varchar(100) NOT NULL,
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sequence`
--

CREATE TABLE `sequence` (
  `SEQ_NAME` varchar(50) NOT NULL,
  `SEQ_COUNT` decimal(38,0) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `seven_aggregator_request`
--

CREATE TABLE `seven_aggregator_request` (
  `id` bigint(20) NOT NULL,
  `amount` decimal(10,2) NOT NULL,
  `request_name` varchar(200) NOT NULL,
  `amount_small` bigint(20) UNSIGNED NOT NULL,
  `currency` varchar(50) NOT NULL,
  `user` varchar(100) NOT NULL,
  `payment_strategy` enum('strictSingle','flexibleMultiple') DEFAULT NULL,
  `transactionType` enum('reserveFunds','credit') DEFAULT NULL,
  `payment_id` varchar(200) NOT NULL,
  `transaction_id` varchar(200) DEFAULT NULL,
  `source_id` varchar(250) NOT NULL,
  `reference_id` varchar(250) NOT NULL,
  `tp_token` text DEFAULT NULL,
  `ticket_info` text DEFAULT NULL,
  `security_hash` varchar(250) DEFAULT NULL,
  `club_uuid` varchar(250) NOT NULL,
  `status` int(11) DEFAULT 0,
  `aggregator_status` enum('cancelled','completed','processing') DEFAULT 'processing',
  `created_by` varchar(200) NOT NULL,
  `date_created` datetime NOT NULL,
  `date_modified` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `shop_deposits`
--

CREATE TABLE `shop_deposits` (
  `transaction_id` bigint(20) NOT NULL,
  `msisdn` varchar(35) NOT NULL,
  `code` varchar(100) NOT NULL,
  `network` varchar(100) NOT NULL,
  `amount` decimal(53,2) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `depositor` varchar(100) NOT NULL,
  `created_by` varchar(120) NOT NULL,
  `approved_by` varchar(120) DEFAULT NULL,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `shop_withdrawals`
--

CREATE TABLE `shop_withdrawals` (
  `withdrawal_id` bigint(20) NOT NULL,
  `msisdn` varchar(35) NOT NULL,
  `amount` decimal(64,2) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `created_by` varchar(120) NOT NULL,
  `approved_by` varchar(120) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `speed_dial_history`
--

CREATE TABLE `speed_dial_history` (
  `id` bigint(20) NOT NULL,
  `profile_id` bigint(20) NOT NULL,
  `msisdn` varchar(45) NOT NULL,
  `source` varchar(200) NOT NULL,
  `header_info` longtext DEFAULT NULL,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `speed_dial_profile`
--

CREATE TABLE `speed_dial_profile` (
  `id` bigint(20) NOT NULL,
  `profile_id` bigint(20) NOT NULL,
  `msisdn` varchar(45) NOT NULL,
  `status` int(11) DEFAULT 1,
  `date_created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sport`
--

CREATE TABLE `sport` (
  `sport_id` int(11) NOT NULL,
  `sport_name` varchar(50) NOT NULL,
  `created_by` varchar(70) NOT NULL,
  `priority` int(11) NOT NULL,
  `betradar_sport_id` int(10) NOT NULL DEFAULT 0,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sqlmapfile`
--

CREATE TABLE `sqlmapfile` (
  `data` longblob DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `talksport`
--

CREATE TABLE `talksport` (
  `talksport_id` int(11) NOT NULL,
  `parent_match_id` bigint(20) NOT NULL,
  `stream_url` varchar(160) NOT NULL,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `team`
--

CREATE TABLE `team` (
  `team_id` int(11) NOT NULL,
  `team_name` varchar(50) NOT NULL,
  `created_by` varchar(70) NOT NULL,
  `betradar_team_id` int(10) NOT NULL DEFAULT 0,
  `betradar_super_id` int(10) NOT NULL DEFAULT 0,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ticket_settlement`
--

CREATE TABLE `ticket_settlement` (
  `id` int(11) NOT NULL,
  `profile_id` int(11) NOT NULL,
  `bet_id` bigint(20) NOT NULL,
  `trx_id` bigint(20) NOT NULL,
  `amount` float NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tournament`
--

CREATE TABLE `tournament` (
  `profile_balance_id` int(10) NOT NULL,
  `profile_id` bigint(20) NOT NULL,
  `balance` decimal(10,2) NOT NULL,
  `transaction_id` bigint(20) NOT NULL,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT current_timestamp(),
  `bonus_balance` decimal(10,2) DEFAULT 0.00,
  `old_balance` double(10,2) DEFAULT NULL,
  `host` varchar(100) DEFAULT NULL,
  `user` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `traffic_analytic`
--

CREATE TABLE `traffic_analytic` (
  `id` bigint(20) NOT NULL,
  `url` varchar(60) DEFAULT NULL,
  `ref` varchar(20) DEFAULT NULL,
  `user_agent` varchar(160) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `transaction`
--

CREATE TABLE `transaction` (
  `id` int(11) NOT NULL,
  `profile_id` bigint(20) NOT NULL,
  `account` varchar(50) NOT NULL,
  `iscredit` smallint(1) NOT NULL,
  `reference` varchar(50) NOT NULL,
  `amount` decimal(10,0) NOT NULL,
  `running_balance` decimal(10,2) DEFAULT NULL,
  `created_by` varchar(60) NOT NULL,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `status` enum('COMPLETE','PENDING') DEFAULT 'COMPLETE'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `trx_bal`
--

CREATE TABLE `trx_bal` (
  `id` int(11) NOT NULL,
  `trx_id` bigint(20) NOT NULL,
  `profile_id` bigint(20) DEFAULT NULL,
  `balance` float DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `types`
--

CREATE TABLE `types` (
  `bet_type_id` int(11) NOT NULL,
  `name` varchar(70) NOT NULL,
  `created_by` varchar(70) NOT NULL,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `sub_type_id` int(11) NOT NULL,
  `short_name` varchar(10) NOT NULL,
  `priority` int(10) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` bigint(20) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password_hash` text NOT NULL,
  `email` text NOT NULL,
  `auth_key` text NOT NULL,
  `password_reset_token` text DEFAULT NULL,
  `status` varchar(5) NOT NULL,
  `created_at` text NOT NULL,
  `updated_at` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_bet_cancel`
--

CREATE TABLE `user_bet_cancel` (
  `id` int(11) NOT NULL,
  `bet_id` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `created_by` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ux_categories`
--

CREATE TABLE `ux_categories` (
  `ux_id` int(11) NOT NULL,
  `sport_name` varchar(60) NOT NULL,
  `category` varchar(60) NOT NULL,
  `competition_name` varchar(60) NOT NULL,
  `sport_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `competition_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ux_todays_highlights`
--

CREATE TABLE `ux_todays_highlights` (
  `highlight_id` int(11) NOT NULL,
  `sport_id` int(11) DEFAULT 79,
  `m_priority` int(10) NOT NULL DEFAULT 0,
  `priority` int(11) NOT NULL,
  `side_bets` int(11) NOT NULL,
  `sub_type_id` int(11) NOT NULL,
  `home_odd` varchar(50) NOT NULL,
  `neutral_odd` varchar(50) NOT NULL,
  `away_odd` varchar(50) NOT NULL,
  `double_chance_12_odd` float(10,2) DEFAULT NULL,
  `double_chance_x2_odd` float(10,2) DEFAULT NULL,
  `double_chance_1x_odd` float(10,2) DEFAULT NULL,
  `over_25_odd` float(10,2) DEFAULT NULL,
  `under_25_odd` float(10,2) DEFAULT NULL,
  `game_id` int(11) NOT NULL,
  `match_id` int(11) NOT NULL,
  `start_time` datetime NOT NULL,
  `away_team` varchar(50) NOT NULL,
  `home_team` varchar(50) NOT NULL,
  `parent_match_id` int(11) NOT NULL,
  `category` varchar(60) NOT NULL,
  `competition_name` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `virtual_competition`
--

CREATE TABLE `virtual_competition` (
  `v_competition_id` int(11) NOT NULL,
  `competition_name` varchar(120) NOT NULL,
  `category` varchar(120) NOT NULL,
  `status` smallint(1) NOT NULL,
  `category_id` int(10) NOT NULL DEFAULT 0,
  `sport_id` int(11) NOT NULL,
  `created_by` varchar(70) NOT NULL,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `priority` int(10) DEFAULT 0,
  `max_stake` decimal(10,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `virtual_event_odd`
--

CREATE TABLE `virtual_event_odd` (
  `v_event_odd_id` int(11) NOT NULL,
  `parent_virtual_id` int(20) NOT NULL,
  `sub_type_id` int(11) DEFAULT NULL,
  `max_bet` decimal(10,0) NOT NULL,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `odd_key` varchar(20) NOT NULL,
  `odd_value` varchar(20) DEFAULT NULL,
  `odd_alias` varchar(20) DEFAULT NULL,
  `special_bet_value` varchar(20) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `virtual_latest_alive`
--

CREATE TABLE `virtual_latest_alive` (
  `alive_id` bigint(20) NOT NULL,
  `alive_string` text DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` timestamp NULL DEFAULT NULL,
  `old_alive_string` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `virtual_match`
--

CREATE TABLE `virtual_match` (
  `v_match_id` int(11) NOT NULL,
  `parent_virtual_id` int(20) NOT NULL,
  `home_team` varchar(50) NOT NULL,
  `away_team` varchar(50) NOT NULL,
  `start_time` datetime NOT NULL,
  `competition_id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `instance_id` int(10) NOT NULL DEFAULT 0,
  `bet_closure` datetime NOT NULL,
  `created_by` varchar(60) NOT NULL,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `result` varchar(45) DEFAULT NULL,
  `ht_score` varchar(5) DEFAULT NULL,
  `ft_score` varchar(5) DEFAULT NULL,
  `completed` tinyint(1) DEFAULT 0,
  `priority` int(10) DEFAULT 50
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `virtual_odds`
--

CREATE TABLE `virtual_odds` (
  `live_odds_change_id` bigint(20) NOT NULL,
  `parent_match_id` bigint(20) NOT NULL,
  `sub_type_id` int(10) NOT NULL,
  `odd_key` varchar(20) NOT NULL,
  `odd_value` varchar(20) NOT NULL,
  `special_bet_value` varchar(20) DEFAULT NULL,
  `match_time` tinytext NOT NULL,
  `score` tinytext NOT NULL,
  `bet_status` tinytext NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 0,
  `market_active` tinyint(1) NOT NULL DEFAULT 0,
  `odd_active` tinyint(1) NOT NULL DEFAULT 0,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `sequence_no` bigint(20) NOT NULL DEFAULT 0,
  `betradar_timestamp` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `virtual_odds_meta`
--

CREATE TABLE `virtual_odds_meta` (
  `live_odds_meta_id` bigint(20) NOT NULL,
  `parent_match_id` bigint(20) NOT NULL,
  `match_time` int(10) NOT NULL DEFAULT 0,
  `score` varchar(60) DEFAULT NULL,
  `bet_status` varchar(60) DEFAULT NULL,
  `match_status` varchar(20) DEFAULT NULL,
  `event_status` varchar(60) DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 0,
  `sequence_no` bigint(20) NOT NULL DEFAULT 0,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `betradar_timestamp` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `virtual_odd_type`
--

CREATE TABLE `virtual_odd_type` (
  `v_bet_type_id` int(11) NOT NULL,
  `name` varchar(70) NOT NULL,
  `created_by` varchar(70) NOT NULL,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `sub_type_id` int(11) NOT NULL,
  `live_bet` tinyint(4) DEFAULT 0,
  `short_name` varchar(10) NOT NULL,
  `priority` decimal(10,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `virtual_outcome`
--

CREATE TABLE `virtual_outcome` (
  `v_match_result_id` int(11) NOT NULL,
  `sub_type_id` int(20) NOT NULL,
  `parent_virtual_id` int(20) NOT NULL,
  `special_bet_value` varchar(20) NOT NULL DEFAULT '',
  `live_bet` tinyint(4) DEFAULT 0,
  `created_by` varchar(70) NOT NULL,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT current_timestamp(),
  `status` int(1) DEFAULT 0,
  `approved_by` varchar(70) DEFAULT NULL,
  `approved_status` tinyint(4) DEFAULT 0,
  `date_approved` datetime DEFAULT NULL,
  `winning_outcome` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `virtual_sport`
--

CREATE TABLE `virtual_sport` (
  `v_sport_id` int(11) NOT NULL,
  `sport_name` varchar(50) NOT NULL,
  `created_by` varchar(70) NOT NULL,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `void_bet_slip`
--

CREATE TABLE `void_bet_slip` (
  `bet_slip_id` int(11) NOT NULL,
  `parent_match_id` int(11) NOT NULL,
  `bet_id` int(11) NOT NULL,
  `bet_pick` varchar(250) NOT NULL,
  `special_bet_value` varchar(20) NOT NULL DEFAULT '',
  `total_games` int(10) NOT NULL,
  `odd_value` decimal(10,2) NOT NULL,
  `win` tinyint(1) NOT NULL,
  `live_bet` tinyint(1) NOT NULL DEFAULT 0,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT current_timestamp(),
  `status` int(1) NOT NULL,
  `sub_type_id` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `winner`
--

CREATE TABLE `winner` (
  `winner_id` int(11) NOT NULL,
  `bet_id` int(11) NOT NULL,
  `bet_amount` decimal(10,0) NOT NULL,
  `win_amount` decimal(10,0) NOT NULL,
  `profile_id` bigint(20) NOT NULL,
  `credit_status` smallint(1) NOT NULL DEFAULT 0,
  `created_by` varchar(70) NOT NULL,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `withdrawal`
--

CREATE TABLE `withdrawal` (
  `withdrawal_id` int(11) NOT NULL,
  `inbox_id` int(11) DEFAULT NULL,
  `msisdn` varchar(25) NOT NULL,
  `raw_text` varchar(200) DEFAULT NULL,
  `amount` decimal(64,2) NOT NULL,
  `reference` varchar(50) NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `created_by` varchar(45) NOT NULL,
  `status` enum('PROCESSING','QUEUED','SUCCESS','FAILED','TRX_SUCCESS','SENT','CANCELLED','REVERSED') DEFAULT NULL,
  `response_status` varchar(250) DEFAULT NULL,
  `provider_reference` varchar(250) DEFAULT NULL,
  `number_of_sends` int(11) NOT NULL DEFAULT 0,
  `charge` float NOT NULL DEFAULT 30,
  `max_withdraw` float NOT NULL DEFAULT 10000,
  `network` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `withdraw_references`
--

CREATE TABLE `withdraw_references` (
  `mpesa_ref` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `account_freeze`
--
ALTER TABLE `account_freeze`
  ADD PRIMARY KEY (`account_freeze_id`),
  ADD UNIQUE KEY `msisdn_status` (`msisdn`,`status`);

--
-- Indexes for table `airtel_money`
--
ALTER TABLE `airtel_money`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `msisdn_airtel_money_code` (`msisdn`,`airtel_money_code`),
  ADD UNIQUE KEY `airtel_money_code` (`airtel_money_code`);

--
-- Indexes for table `airtel_money_rate`
--
ALTER TABLE `airtel_money_rate`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `airtel_subs_blast`
--
ALTER TABLE `airtel_subs_blast`
  ADD PRIMARY KEY (`id`),
  ADD KEY `msisdn` (`msisdn`),
  ADD KEY `status` (`status`);

--
-- Indexes for table `android_auth`
--
ALTER TABLE `android_auth`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `archive_bet`
--
ALTER TABLE `archive_bet`
  ADD PRIMARY KEY (`bet_id`),
  ADD KEY `profile_id` (`profile_id`),
  ADD KEY `reference` (`reference`),
  ADD KEY `created` (`created`),
  ADD KEY `prof_idx` (`profile_id`),
  ADD KEY `created_2` (`created`),
  ADD KEY `profile_id_2` (`profile_id`);

--
-- Indexes for table `archive_bet_slip`
--
ALTER TABLE `archive_bet_slip`
  ADD PRIMARY KEY (`bet_slip_id`),
  ADD KEY `bet_id` (`bet_id`),
  ADD KEY `parent_match_id` (`parent_match_id`),
  ADD KEY `bet_pick` (`bet_pick`);

--
-- Indexes for table `archive_bonus_bet`
--
ALTER TABLE `archive_bonus_bet`
  ADD PRIMARY KEY (`bonus_bet_id`),
  ADD KEY `bet_id` (`bet_id`),
  ADD KEY `profile_bonus_id` (`profile_bonus_id`),
  ADD KEY `created` (`created`);

--
-- Indexes for table `archive_bonus_trx`
--
ALTER TABLE `archive_bonus_trx`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `profile_id_reference_iscredit_created_by` (`profile_id`,`reference`,`iscredit`,`created_by`),
  ADD KEY `profile_id` (`profile_id`),
  ADD KEY `reference` (`reference`),
  ADD KEY `created_by` (`created_by`);

--
-- Indexes for table `archive_jackpot_bet`
--
ALTER TABLE `archive_jackpot_bet`
  ADD PRIMARY KEY (`jackpot_bet_id`),
  ADD KEY `bet_id` (`bet_id`),
  ADD KEY `jackpot_event_id` (`jackpot_event_id`);

--
-- Indexes for table `archive_jackpot_trx`
--
ALTER TABLE `archive_jackpot_trx`
  ADD PRIMARY KEY (`jackpot_trx_id`),
  ADD KEY `jackpot_event_id` (`jackpot_event_id`),
  ADD KEY `trx_id` (`trx_id`);

--
-- Indexes for table `archive_jackpot_winner`
--
ALTER TABLE `archive_jackpot_winner`
  ADD PRIMARY KEY (`jackpot_winner_id`),
  ADD UNIQUE KEY `jackpot_event_id_bet_id_total_games_correct_msisdn` (`jackpot_event_id`,`bet_id`,`total_games_correct`),
  ADD KEY `bet_id` (`bet_id`),
  ADD KEY `jackpot_event_id` (`jackpot_event_id`);

--
-- Indexes for table `archive_match`
--
ALTER TABLE `archive_match`
  ADD PRIMARY KEY (`match_id`),
  ADD UNIQUE KEY `parent_match_ids` (`parent_match_id`),
  ADD KEY `competition_id` (`competition_id`),
  ADD KEY `parent_match_id` (`parent_match_id`),
  ADD KEY `instance_id` (`instance_id`),
  ADD KEY `start_time` (`start_time`),
  ADD KEY `status` (`status`);

--
-- Indexes for table `archive_transaction`
--
ALTER TABLE `archive_transaction`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `profile_id_reference_iscredit` (`profile_id`,`reference`,`iscredit`),
  ADD KEY `profile_id` (`profile_id`),
  ADD KEY `reference` (`reference`);

--
-- Indexes for table `arch_live_match`
--
ALTER TABLE `arch_live_match`
  ADD PRIMARY KEY (`match_id`),
  ADD UNIQUE KEY `game_id` (`game_id`),
  ADD UNIQUE KEY `parent_match_ids` (`parent_match_id`),
  ADD KEY `competition_id` (`competition_id`),
  ADD KEY `parent_match_id` (`parent_match_id`),
  ADD KEY `instance_id` (`instance_id`),
  ADD KEY `start_time` (`start_time`);

--
-- Indexes for table `auth_assignment`
--
ALTER TABLE `auth_assignment`
  ADD PRIMARY KEY (`item_name`,`user_id`);

--
-- Indexes for table `auth_item`
--
ALTER TABLE `auth_item`
  ADD PRIMARY KEY (`name`),
  ADD KEY `rule_name` (`rule_name`),
  ADD KEY `idx-auth_item-type` (`type`);

--
-- Indexes for table `auth_item_child`
--
ALTER TABLE `auth_item_child`
  ADD PRIMARY KEY (`parent`,`child`),
  ADD KEY `child` (`child`);

--
-- Indexes for table `auth_rule`
--
ALTER TABLE `auth_rule`
  ADD PRIMARY KEY (`name`);

--
-- Indexes for table `auth_token`
--
ALTER TABLE `auth_token`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `backup_profile`
--
ALTER TABLE `backup_profile`
  ADD PRIMARY KEY (`profile_id`),
  ADD UNIQUE KEY `msisdn` (`msisdn`);

--
-- Indexes for table `bb`
--
ALTER TABLE `bb`
  ADD PRIMARY KEY (`bet_id`),
  ADD KEY `profile_id` (`profile_id`),
  ADD KEY `reference` (`reference`);

--
-- Indexes for table `bet`
--
ALTER TABLE `bet`
  ADD PRIMARY KEY (`bet_id`),
  ADD KEY `profile_id` (`profile_id`),
  ADD KEY `reference` (`reference`),
  ADD KEY `created` (`created`),
  ADD KEY `prof_idx` (`profile_id`),
  ADD KEY `created_2` (`created`),
  ADD KEY `profile_id_2` (`profile_id`);

--
-- Indexes for table `betting_control`
--
ALTER TABLE `betting_control`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bet_discount`
--
ALTER TABLE `bet_discount`
  ADD PRIMARY KEY (`bet_discount_id`),
  ADD UNIQUE KEY `bet_id` (`bet_id`);

--
-- Indexes for table `bet_slip`
--
ALTER TABLE `bet_slip`
  ADD PRIMARY KEY (`bet_slip_id`),
  ADD KEY `bet_id` (`bet_id`),
  ADD KEY `parent_match_id` (`parent_match_id`),
  ADD KEY `bet_pick` (`bet_pick`);

--
-- Indexes for table `bet_slip_check`
--
ALTER TABLE `bet_slip_check`
  ADD PRIMARY KEY (`bet_slip_id`),
  ADD KEY `bet_id` (`bet_id`),
  ADD KEY `parent_match_id` (`parent_match_id`),
  ADD KEY `bet_pick` (`bet_pick`);

--
-- Indexes for table `bet_slip_temp`
--
ALTER TABLE `bet_slip_temp`
  ADD PRIMARY KEY (`bet_slip_id`),
  ADD KEY `match_id` (`match_id`,`reference_id`),
  ADD KEY `reference_id` (`reference_id`),
  ADD KEY `bet_type` (`bet_type`);

--
-- Indexes for table `bet_status_changes`
--
ALTER TABLE `bet_status_changes`
  ADD PRIMARY KEY (`status_change_id`),
  ADD KEY `parent_match_id` (`parent_match_id`);

--
-- Indexes for table `bleague_competition`
--
ALTER TABLE `bleague_competition`
  ADD PRIMARY KEY (`competition_id`),
  ADD UNIQUE KEY `competition_name_category_sport_id` (`competition_name`,`category`,`sport_id`),
  ADD KEY `sport_id` (`sport_id`);

--
-- Indexes for table `bleague_event_odd`
--
ALTER TABLE `bleague_event_odd`
  ADD PRIMARY KEY (`event_odd_id`),
  ADD UNIQUE KEY `parent_match_id_sub_type_id_odd_key_special_bet_value` (`parent_match_id`,`sub_type_id`,`odd_key`,`special_bet_value`),
  ADD KEY `match_id` (`parent_match_id`),
  ADD KEY `bet_type_id` (`sub_type_id`);

--
-- Indexes for table `bleague_match`
--
ALTER TABLE `bleague_match`
  ADD PRIMARY KEY (`match_id`),
  ADD UNIQUE KEY `game_id` (`game_id`),
  ADD UNIQUE KEY `parent_match_ids` (`parent_match_id`),
  ADD KEY `competition_id` (`competition_id`),
  ADD KEY `parent_match_id` (`parent_match_id`);

--
-- Indexes for table `bonus_bet`
--
ALTER TABLE `bonus_bet`
  ADD PRIMARY KEY (`bonus_bet_id`),
  ADD KEY `bet_id` (`bet_id`),
  ADD KEY `profile_bonus_id` (`profile_bonus_id`),
  ADD KEY `created` (`created`);

--
-- Indexes for table `bonus_bet_count`
--
ALTER TABLE `bonus_bet_count`
  ADD PRIMARY KEY (`bonus_bet_count_id`),
  ADD UNIQUE KEY `profile_bonus_id_2` (`profile_bonus_id`,`profile_id`),
  ADD KEY `profile_bonus_id` (`profile_bonus_id`),
  ADD KEY `profile_id` (`profile_id`),
  ADD KEY `created` (`created`);

--
-- Indexes for table `bonus_trx`
--
ALTER TABLE `bonus_trx`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `profile_id_reference_iscredit_created_by` (`profile_id`,`reference`,`iscredit`,`created_by`),
  ADD KEY `profile_id` (`profile_id`),
  ADD KEY `reference` (`reference`),
  ADD KEY `created_by` (`created_by`);

--
-- Indexes for table `card_summary`
--
ALTER TABLE `card_summary`
  ADD PRIMARY KEY (`card_summary_id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `parent_match_id` (`parent_match_id`),
  ADD KEY `type` (`type`),
  ADD KEY `team` (`team`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`category_id`),
  ADD UNIQUE KEY `category_name_category_sport_id` (`category_name`,`sport_id`),
  ADD KEY `sport_id` (`sport_id`),
  ADD KEY `category_name` (`category_name`);

--
-- Indexes for table `competition`
--
ALTER TABLE `competition`
  ADD PRIMARY KEY (`competition_id`),
  ADD UNIQUE KEY `competition_name_category_sport_id` (`competition_name`,`category`,`sport_id`),
  ADD KEY `sport_id` (`sport_id`),
  ADD KEY `category_id` (`category_id`),
  ADD KEY `competition_name` (`competition_name`);

--
-- Indexes for table `delivery_report`
--
ALTER TABLE `delivery_report`
  ADD PRIMARY KEY (`delivery_report_id`),
  ADD UNIQUE KEY `correlator_msisdn` (`correlator`,`msisdn`);

--
-- Indexes for table `early_bet_white_list`
--
ALTER TABLE `early_bet_white_list`
  ADD UNIQUE KEY `msisdn` (`msisdn`);

--
-- Indexes for table `event`
--
ALTER TABLE `event`
  ADD PRIMARY KEY (`match_id`),
  ADD UNIQUE KEY `game_id` (`game_id`),
  ADD UNIQUE KEY `parent_match_id` (`parent_match_id`),
  ADD KEY `competition_id` (`competition_id`);

--
-- Indexes for table `event_odd`
--
ALTER TABLE `event_odd`
  ADD PRIMARY KEY (`event_odd_id`),
  ADD UNIQUE KEY `parent_match_id_sub_type_id_odd_key` (`parent_match_id`,`sub_type_id`,`odd_key`,`special_bet_value`),
  ADD KEY `match_id` (`parent_match_id`),
  ADD KEY `bet_type_id` (`sub_type_id`);

--
-- Indexes for table `free_bet`
--
ALTER TABLE `free_bet`
  ADD PRIMARY KEY (`free_bet_id`),
  ADD UNIQUE KEY `profile_id_event_date_status` (`profile_id`,`event_date`,`status`),
  ADD KEY `profile_id` (`profile_id`);

--
-- Indexes for table `free_bet_transactions`
--
ALTER TABLE `free_bet_transactions`
  ADD PRIMARY KEY (`free_bet_transactions_id`),
  ADD UNIQUE KEY `bet_id_profile_id` (`bet_id`,`profile_id`),
  ADD UNIQUE KEY `bet_id` (`bet_id`),
  ADD KEY `free_bet_id` (`free_bet_id`);

--
-- Indexes for table `ga`
--
ALTER TABLE `ga`
  ADD KEY `number` (`number`);

--
-- Indexes for table `game`
--
ALTER TABLE `game`
  ADD PRIMARY KEY (`match_id`),
  ADD UNIQUE KEY `game_id` (`game_id`),
  ADD UNIQUE KEY `parent_match_ids` (`parent_match_id`),
  ADD KEY `competition_id` (`competition_id`),
  ADD KEY `parent_match_id` (`parent_match_id`);

--
-- Indexes for table `game_ids`
--
ALTER TABLE `game_ids`
  ADD PRIMARY KEY (`id`),
  ADD KEY `number` (`number`);

--
-- Indexes for table `game_request`
--
ALTER TABLE `game_request`
  ADD PRIMARY KEY (`request_id`),
  ADD KEY `profile_id` (`profile_id`);

--
-- Indexes for table `gk_bonus_campaign`
--
ALTER TABLE `gk_bonus_campaign`
  ADD PRIMARY KEY (`id`),
  ADD KEY `clabel` (`clabel`);

--
-- Indexes for table `gk_bonus_upload`
--
ALTER TABLE `gk_bonus_upload`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gk_sms`
--
ALTER TABLE `gk_sms`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sdate` (`sdate`),
  ADD KEY `msisdn` (`msisdn`);

--
-- Indexes for table `gk_users`
--
ALTER TABLE `gk_users`
  ADD PRIMARY KEY (`uid`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `gr_profile_player`
--
ALTER TABLE `gr_profile_player`
  ADD PRIMARY KEY (`player_id`),
  ADD UNIQUE KEY `profile_id` (`profile_id`),
  ADD UNIQUE KEY `gr_player_id` (`gr_player_id`);

--
-- Indexes for table `gr_tickets`
--
ALTER TABLE `gr_tickets`
  ADD PRIMARY KEY (`gr_ticket_id`),
  ADD UNIQUE KEY `gr_ticket` (`gr_ticket`),
  ADD KEY `profile_id` (`profile_id`);

--
-- Indexes for table `inactive2_subs`
--
ALTER TABLE `inactive2_subs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `msisdn` (`msisdn`);

--
-- Indexes for table `inactive_profile`
--
ALTER TABLE `inactive_profile`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `profile_id` (`profile_id`);

--
-- Indexes for table `inbox`
--
ALTER TABLE `inbox`
  ADD PRIMARY KEY (`inbox_id`);

--
-- Indexes for table `jackpot_bet`
--
ALTER TABLE `jackpot_bet`
  ADD PRIMARY KEY (`jackpot_bet_id`),
  ADD KEY `bet_id` (`bet_id`),
  ADD KEY `jackpot_event_id` (`jackpot_event_id`);

--
-- Indexes for table `jackpot_event`
--
ALTER TABLE `jackpot_event`
  ADD PRIMARY KEY (`jackpot_event_id`),
  ADD UNIQUE KEY `jackpot_name` (`jackpot_name`),
  ADD KEY `jackpot_type` (`jackpot_type`);

--
-- Indexes for table `jackpot_match`
--
ALTER TABLE `jackpot_match`
  ADD PRIMARY KEY (`jackpot_match_id`),
  ADD UNIQUE KEY `jackpot_event_id_parent_match_id` (`jackpot_event_id`,`parent_match_id`),
  ADD KEY `parent_match_id` (`parent_match_id`);

--
-- Indexes for table `jackpot_trx`
--
ALTER TABLE `jackpot_trx`
  ADD PRIMARY KEY (`jackpot_trx_id`),
  ADD KEY `jackpot_event_id` (`jackpot_event_id`),
  ADD KEY `trx_id` (`trx_id`);

--
-- Indexes for table `jackpot_type`
--
ALTER TABLE `jackpot_type`
  ADD PRIMARY KEY (`jackpot_type_id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `jackpot_winner`
--
ALTER TABLE `jackpot_winner`
  ADD PRIMARY KEY (`jackpot_winner_id`),
  ADD UNIQUE KEY `jackpot_event_id_bet_id_total_games_correct_msisdn` (`jackpot_event_id`,`bet_id`,`total_games_correct`),
  ADD KEY `bet_id` (`bet_id`),
  ADD KEY `jackpot_event_id` (`jackpot_event_id`);

--
-- Indexes for table `jpbonus_award`
--
ALTER TABLE `jpbonus_award`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `jackpot_event_id_total_games_correct` (`jackpot_event_id`,`total_games_correct`);

--
-- Indexes for table `jp_bet_cancel_data`
--
ALTER TABLE `jp_bet_cancel_data`
  ADD PRIMARY KEY (`id`),
  ADD KEY `profile_id` (`profile_id`),
  ADD KEY `bet_id` (`bet_id`),
  ADD KEY `status` (`status`);

--
-- Indexes for table `latest_alive`
--
ALTER TABLE `latest_alive`
  ADD PRIMARY KEY (`alive_id`);

--
-- Indexes for table `live_betting_control`
--
ALTER TABLE `live_betting_control`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `live_enabled_market`
--
ALTER TABLE `live_enabled_market`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `live_match`
--
ALTER TABLE `live_match`
  ADD PRIMARY KEY (`match_id`),
  ADD UNIQUE KEY `game_id` (`game_id`),
  ADD UNIQUE KEY `parent_match_ids` (`parent_match_id`),
  ADD KEY `competition_id` (`competition_id`),
  ADD KEY `instance_id` (`instance_id`),
  ADD KEY `start_time` (`start_time`),
  ADD KEY `match_time` (`match_time`),
  ADD KEY `event_status` (`event_status`),
  ADD KEY `active` (`active`),
  ADD KEY `match_status` (`match_status`);

--
-- Indexes for table `live_meta_history`
--
ALTER TABLE `live_meta_history`
  ADD PRIMARY KEY (`live_odds_meta_id`),
  ADD KEY `parent_match_id` (`parent_match_id`),
  ADD KEY `betradar_timestamp` (`betradar_timestamp`),
  ADD KEY `sequence_no` (`sequence_no`);

--
-- Indexes for table `live_odds`
--
ALTER TABLE `live_odds`
  ADD PRIMARY KEY (`live_odds_change_id`),
  ADD UNIQUE KEY `sub_type_ids` (`parent_match_id`,`sub_type_id`,`odd_key`,`special_bet_value`),
  ADD KEY `parent_match_id` (`parent_match_id`),
  ADD KEY `sub_type_id` (`sub_type_id`),
  ADD KEY `odd_key` (`odd_key`),
  ADD KEY `market_active` (`market_active`),
  ADD KEY `odd_active` (`odd_active`);

--
-- Indexes for table `live_odds_change`
--
ALTER TABLE `live_odds_change`
  ADD PRIMARY KEY (`live_odds_change_id`),
  ADD UNIQUE KEY `odd_unique_key` (`parent_match_id`,`sub_type_id`,`special_bet_value`,`odd_key`),
  ADD KEY `betradar_timestamp` (`betradar_timestamp`),
  ADD KEY `parent_match_on` (`parent_match_id`),
  ADD KEY `sub_type_id` (`sub_type_id`),
  ADD KEY `market_active` (`market_active`),
  ADD KEY `odd_active` (`active`),
  ADD KEY `odd_key` (`odd_key`),
  ADD KEY `special_bet_value` (`special_bet_value`);

--
-- Indexes for table `live_odds_meta`
--
ALTER TABLE `live_odds_meta`
  ADD PRIMARY KEY (`live_odds_meta_id`),
  ADD UNIQUE KEY `parent_match_id_e` (`parent_match_id`),
  ADD KEY `parent_match_id` (`parent_match_id`),
  ADD KEY `betradar_timestamp` (`betradar_timestamp`),
  ADD KEY `sequence_no` (`sequence_no`),
  ADD KEY `event_status` (`event_status`),
  ADD KEY `match_time` (`match_time`),
  ADD KEY `bet_status` (`bet_status`);

--
-- Indexes for table `logbetcancel`
--
ALTER TABLE `logbetcancel`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ls_betmatch`
--
ALTER TABLE `ls_betmatch`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `betradar_match_id` (`betradar_match_id`,`parent_match_id`);

--
-- Indexes for table `ls_card`
--
ALTER TABLE `ls_card`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ls_category`
--
ALTER TABLE `ls_category`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `betradar_category_id` (`betradar_category_id`,`category_name`);

--
-- Indexes for table `ls_city`
--
ALTER TABLE `ls_city`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `city_id` (`city_id`);

--
-- Indexes for table `ls_country`
--
ALTER TABLE `ls_country`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `country_id` (`country_id`);

--
-- Indexes for table `ls_goal`
--
ALTER TABLE `ls_goal`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `goal_id` (`goal_id`,`time`);

--
-- Indexes for table `ls_line_up`
--
ALTER TABLE `ls_line_up`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ls_livescore_data`
--
ALTER TABLE `ls_livescore_data`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ls_match_detail`
--
ALTER TABLE `ls_match_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ls_player`
--
ALTER TABLE `ls_player`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ls_referee`
--
ALTER TABLE `ls_referee`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ls_scores`
--
ALTER TABLE `ls_scores`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ls_sport`
--
ALTER TABLE `ls_sport`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `betradar_sport_id` (`betradar_sport_id`);

--
-- Indexes for table `ls_substitution`
--
ALTER TABLE `ls_substitution`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ls_team`
--
ALTER TABLE `ls_team`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ls_tournament`
--
ALTER TABLE `ls_tournament`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `betradar_tournament_id` (`betradar_tournament_id`);

--
-- Indexes for table `ls_venue`
--
ALTER TABLE `ls_venue`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `market_priority`
--
ALTER TABLE `market_priority`
  ADD PRIMARY KEY (`id`),
  ADD KEY `subtype_inx` (`sub_type_id`);

--
-- Indexes for table `match`
--
ALTER TABLE `match`
  ADD PRIMARY KEY (`match_id`),
  ADD UNIQUE KEY `game_id` (`game_id`),
  ADD UNIQUE KEY `parent_match_ids` (`parent_match_id`),
  ADD KEY `competition_id` (`competition_id`),
  ADD KEY `parent_match_id` (`parent_match_id`),
  ADD KEY `instance_id` (`instance_id`),
  ADD KEY `start_time` (`start_time`),
  ADD KEY `status` (`status`);

--
-- Indexes for table `matchid`
--
ALTER TABLE `matchid`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `match_id` (`match_id`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parent` (`parent`);

--
-- Indexes for table `migration`
--
ALTER TABLE `migration`
  ADD PRIMARY KEY (`version`);

--
-- Indexes for table `missed_withdrawals`
--
ALTER TABLE `missed_withdrawals`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mobile_app_profile`
--
ALTER TABLE `mobile_app_profile`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `profile_id_token_id` (`profile_id`,`token_id`),
  ADD UNIQUE KEY `profile_id` (`profile_id`);

--
-- Indexes for table `mobile_app_version`
--
ALTER TABLE `mobile_app_version`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `status` (`status`);

--
-- Indexes for table `mpesa_rate`
--
ALTER TABLE `mpesa_rate`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mpesa_transaction`
--
ALTER TABLE `mpesa_transaction`
  ADD PRIMARY KEY (`mpesa_transaction_id`),
  ADD UNIQUE KEY `mpesa_code` (`mpesa_code`),
  ADD UNIQUE KEY `mpesa_code_msisdn` (`mpesa_code`,`msisdn`),
  ADD KEY `msisdn` (`msisdn`),
  ADD KEY `mpesa_code_index` (`mpesa_code`),
  ADD KEY `created` (`created`);

--
-- Indexes for table `mts_exception`
--
ALTER TABLE `mts_exception`
  ADD PRIMARY KEY (`mts_exception_id`);

--
-- Indexes for table `mts_ticket_submit`
--
ALTER TABLE `mts_ticket_submit`
  ADD PRIMARY KEY (`mts_ticket_submit_id`),
  ADD KEY `bet_id` (`bet_id`),
  ADD KEY `status` (`status`),
  ADD KEY `response` (`response`);

--
-- Indexes for table `mts_validation_code`
--
ALTER TABLE `mts_validation_code`
  ADD PRIMARY KEY (`mts_validation_code_id`);

--
-- Indexes for table `network_charge_range`
--
ALTER TABLE `network_charge_range`
  ADD PRIMARY KEY (`network_charge_range_id`),
  ADD UNIQUE KEY `network_min_max` (`network`,`minimum`,`maximum`);

--
-- Indexes for table `nums_pro`
--
ALTER TABLE `nums_pro`
  ADD PRIMARY KEY (`pro_id`),
  ADD UNIQUE KEY `profile_id` (`profile_id`);

--
-- Indexes for table `odds_history`
--
ALTER TABLE `odds_history`
  ADD PRIMARY KEY (`odd_history_id`);

--
-- Indexes for table `odds_subtype`
--
ALTER TABLE `odds_subtype`
  ADD PRIMARY KEY (`odds_subtype_id`),
  ADD UNIQUE KEY `sub_type_id` (`sub_type_id`);

--
-- Indexes for table `odd_history`
--
ALTER TABLE `odd_history`
  ADD PRIMARY KEY (`odd_history_id`);

--
-- Indexes for table `odd_key_alias`
--
ALTER TABLE `odd_key_alias`
  ADD PRIMARY KEY (`odd_key_alias_id`),
  ADD KEY `sub_type_id` (`sub_type_id`);

--
-- Indexes for table `odd_type`
--
ALTER TABLE `odd_type`
  ADD PRIMARY KEY (`bet_type_id`),
  ADD UNIQUE KEY `parent_match_id` (`parent_match_id`,`sub_type_id`,`live_bet`),
  ADD KEY `sub_type_id` (`sub_type_id`),
  ADD KEY `name` (`name`),
  ADD KEY `special_bet_value` (`special_bet_value`),
  ADD KEY `parent_match_id_2` (`parent_match_id`),
  ADD KEY `live_bet` (`live_bet`);

--
-- Indexes for table `odd_type_group`
--
ALTER TABLE `odd_type_group`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `sub_type_id` (`sub_type_id`,`group_name`),
  ADD KEY `group_name` (`group_name`),
  ADD KEY `sub_type_id_2` (`sub_type_id`,`group_name`),
  ADD KEY `sub_type_id_3` (`sub_type_id`);

--
-- Indexes for table `odd_type_old`
--
ALTER TABLE `odd_type_old`
  ADD PRIMARY KEY (`bet_type_id`),
  ADD KEY `sub_type_id` (`sub_type_id`),
  ADD KEY `name` (`name`),
  ADD KEY `special_bet_value` (`special_bet_value`);

--
-- Indexes for table `outbox`
--
ALTER TABLE `outbox`
  ADD PRIMARY KEY (`outbox_id`),
  ADD KEY `fk_outbox_profile_idx` (`profile_id`),
  ADD KEY `date_created` (`date_created`);

--
-- Indexes for table `outcome`
--
ALTER TABLE `outcome`
  ADD PRIMARY KEY (`match_result_id`),
  ADD UNIQUE KEY `parent_match_sub_type_win_outcome_sbvalue_livebet` (`parent_match_id`,`sub_type_id`,`winning_outcome`,`special_bet_value`,`live_bet`),
  ADD KEY `sub_type_id` (`sub_type_id`),
  ADD KEY `parent_match_id` (`parent_match_id`),
  ADD KEY `winning_outcome` (`winning_outcome`),
  ADD KEY `outcome_id` (`outcome_id`);

--
-- Indexes for table `outcome_backup`
--
ALTER TABLE `outcome_backup`
  ADD PRIMARY KEY (`match_result_id`),
  ADD UNIQUE KEY `parent_match_sub_type_win_outcome_sbvalue_livebet` (`parent_match_id`,`sub_type_id`,`winning_outcome`,`special_bet_value`,`live_bet`),
  ADD KEY `sub_type_id` (`sub_type_id`),
  ADD KEY `parent_match_id` (`parent_match_id`),
  ADD KEY `winning_outcome` (`winning_outcome`);

--
-- Indexes for table `outcome_o`
--
ALTER TABLE `outcome_o`
  ADD PRIMARY KEY (`match_result_id`),
  ADD KEY `sub_type_id` (`sub_type_id`),
  ADD KEY `parent_match_id` (`parent_match_id`),
  ADD KEY `winning_outcome` (`winning_outcome`);

--
-- Indexes for table `outcome_old`
--
ALTER TABLE `outcome_old`
  ADD PRIMARY KEY (`match_result_id`),
  ADD KEY `sub_type_id` (`sub_type_id`),
  ADD KEY `parent_match_id` (`parent_match_id`);

--
-- Indexes for table `outcome_test`
--
ALTER TABLE `outcome_test`
  ADD PRIMARY KEY (`match_result_id`),
  ADD UNIQUE KEY `parent_match_id_sub_type_id_winning_outcome` (`parent_match_id`,`sub_type_id`,`winning_outcome`,`special_bet_value`),
  ADD KEY `sub_type_id` (`sub_type_id`),
  ADD KEY `parent_match_id` (`parent_match_id`),
  ADD KEY `winning_outcome` (`winning_outcome`);

--
-- Indexes for table `outright`
--
ALTER TABLE `outright`
  ADD PRIMARY KEY (`outright_id`),
  ADD UNIQUE KEY `parent_outright_id` (`parent_outright_id`),
  ADD KEY `competition_id` (`competition_id`),
  ADD KEY `instance_id` (`instance_id`),
  ADD KEY `event_date` (`event_date`),
  ADD KEY `status` (`status`);

--
-- Indexes for table `outright_competitor`
--
ALTER TABLE `outright_competitor`
  ADD PRIMARY KEY (`competitor_id`),
  ADD UNIQUE KEY `parent_outright_id` (`parent_outright_id`,`betradar_competitor_id`),
  ADD KEY `betradar_competitor_id` (`betradar_competitor_id`),
  ADD KEY `betradar_super_id` (`betradar_super_id`),
  ADD KEY `status` (`status`);

--
-- Indexes for table `outright_odd`
--
ALTER TABLE `outright_odd`
  ADD PRIMARY KEY (`odd_id`),
  ADD UNIQUE KEY `parent_outright_id` (`parent_outright_id`,`betradar_competitor_id`,`odd_type`),
  ADD KEY `betradar_competitor_id` (`betradar_competitor_id`),
  ADD KEY `odd_type` (`odd_type`),
  ADD KEY `status` (`status`);

--
-- Indexes for table `outright_odd_history`
--
ALTER TABLE `outright_odd_history`
  ADD PRIMARY KEY (`odd_id`),
  ADD KEY `betradar_competitor_id` (`betradar_competitor_id`),
  ADD KEY `odd_type` (`odd_type`),
  ADD KEY `status` (`status`);

--
-- Indexes for table `outright_outcome`
--
ALTER TABLE `outright_outcome`
  ADD PRIMARY KEY (`outcome_id`),
  ADD UNIQUE KEY `parent_outright_id` (`parent_outright_id`,`betradar_competitor_id`),
  ADD KEY `betradar_competitor_id` (`betradar_competitor_id`),
  ADD KEY `status` (`status`);

--
-- Indexes for table `paybill_tariff`
--
ALTER TABLE `paybill_tariff`
  ADD PRIMARY KEY (`tariff_id`),
  ADD KEY `fk_paybill_tariff_1_idx` (`network_range_id`);

--
-- Indexes for table `peer_bet`
--
ALTER TABLE `peer_bet`
  ADD PRIMARY KEY (`peer_bet_id`);

--
-- Indexes for table `prematch_disabled_market`
--
ALTER TABLE `prematch_disabled_market`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profile`
--
ALTER TABLE `profile`
  ADD PRIMARY KEY (`profile_id`),
  ADD UNIQUE KEY `msisdn` (`msisdn`);

--
-- Indexes for table `profile_balance`
--
ALTER TABLE `profile_balance`
  ADD PRIMARY KEY (`profile_balance_id`),
  ADD UNIQUE KEY `profile_id_unq` (`profile_id`),
  ADD KEY `profile_id` (`profile_id`),
  ADD KEY `bonus_balance` (`bonus_balance`),
  ADD KEY `transaction_id` (`transaction_id`);

--
-- Indexes for table `profile_bonus`
--
ALTER TABLE `profile_bonus`
  ADD PRIMARY KEY (`profile_bonus_id`),
  ADD KEY `profile_id` (`profile_id`),
  ADD KEY `status` (`status`),
  ADD KEY `referred_msisdn` (`referred_msisdn`);

--
-- Indexes for table `profile_setting`
--
ALTER TABLE `profile_setting`
  ADD PRIMARY KEY (`profile_setting_id`),
  ADD UNIQUE KEY `profile_id` (`profile_id`);

--
-- Indexes for table `profile_settings`
--
ALTER TABLE `profile_settings`
  ADD PRIMARY KEY (`profile_setting_id`),
  ADD UNIQUE KEY `profile_id` (`profile_id`);

--
-- Indexes for table `reconciliation`
--
ALTER TABLE `reconciliation`
  ADD KEY `mpesa_ref` (`mpesa_ref`),
  ADD KEY `transaction_type` (`transaction_type`);

--
-- Indexes for table `recon_references`
--
ALTER TABLE `recon_references`
  ADD KEY `mpesa_ref` (`mpesa_ref`);

--
-- Indexes for table `running_balance`
--
ALTER TABLE `running_balance`
  ADD PRIMARY KEY (`id`),
  ADD KEY `profile_id` (`profile_id`);

--
-- Indexes for table `scorepesa_point`
--
ALTER TABLE `scorepesa_point`
  ADD PRIMARY KEY (`shikabet_point_id`),
  ADD UNIQUE KEY `profile_id` (`profile_id`);

--
-- Indexes for table `scorepesa_point_bet`
--
ALTER TABLE `scorepesa_point_bet`
  ADD PRIMARY KEY (`shikabet_point_bet_id`),
  ADD UNIQUE KEY `bet_id` (`bet_id`),
  ADD UNIQUE KEY `bet_id_betika_point_trx_id` (`bet_id`,`shikabet_point_trx_id`),
  ADD KEY `betika_point_trx_id` (`shikabet_point_trx_id`);

--
-- Indexes for table `scorepesa_point_summary`
--
ALTER TABLE `scorepesa_point_summary`
  ADD PRIMARY KEY (`id`),
  ADD KEY `msisdn` (`msisdn`);

--
-- Indexes for table `scorepesa_point_trx`
--
ALTER TABLE `scorepesa_point_trx`
  ADD PRIMARY KEY (`shikabet_point_trx_id`),
  ADD UNIQUE KEY `trx_id` (`trx_id`);

--
-- Indexes for table `scorepesa_profile_names`
--
ALTER TABLE `scorepesa_profile_names`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `msisdn` (`msisdn`);

--
-- Indexes for table `sequence`
--
ALTER TABLE `sequence`
  ADD PRIMARY KEY (`SEQ_NAME`);

--
-- Indexes for table `seven_aggregator_request`
--
ALTER TABLE `seven_aggregator_request`
  ADD PRIMARY KEY (`id`),
  ADD KEY `payment_id` (`payment_id`),
  ADD KEY `transaction_id` (`transaction_id`);

--
-- Indexes for table `shop_deposits`
--
ALTER TABLE `shop_deposits`
  ADD PRIMARY KEY (`transaction_id`),
  ADD UNIQUE KEY `code` (`code`);

--
-- Indexes for table `shop_withdrawals`
--
ALTER TABLE `shop_withdrawals`
  ADD PRIMARY KEY (`withdrawal_id`);

--
-- Indexes for table `speed_dial_history`
--
ALTER TABLE `speed_dial_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `speed_dial_profile`
--
ALTER TABLE `speed_dial_profile`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `profile_id` (`profile_id`);

--
-- Indexes for table `sport`
--
ALTER TABLE `sport`
  ADD PRIMARY KEY (`sport_id`),
  ADD UNIQUE KEY `u_sport_name` (`sport_name`);

--
-- Indexes for table `talksport`
--
ALTER TABLE `talksport`
  ADD PRIMARY KEY (`talksport_id`);

--
-- Indexes for table `team`
--
ALTER TABLE `team`
  ADD PRIMARY KEY (`team_id`),
  ADD UNIQUE KEY `betradar_super_id` (`betradar_super_id`);

--
-- Indexes for table `ticket_settlement`
--
ALTER TABLE `ticket_settlement`
  ADD PRIMARY KEY (`id`),
  ADD KEY `bet_id` (`bet_id`),
  ADD KEY `profile_id` (`profile_id`);

--
-- Indexes for table `tournament`
--
ALTER TABLE `tournament`
  ADD PRIMARY KEY (`profile_balance_id`),
  ADD KEY `profile_id` (`profile_id`),
  ADD KEY `bonus_balance` (`bonus_balance`),
  ADD KEY `transaction_id` (`transaction_id`);

--
-- Indexes for table `traffic_analytic`
--
ALTER TABLE `traffic_analytic`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transaction`
--
ALTER TABLE `transaction`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `profile_id_reference_iscredit` (`profile_id`,`reference`,`iscredit`),
  ADD KEY `profile_id` (`profile_id`),
  ADD KEY `reference` (`reference`);

--
-- Indexes for table `trx_bal`
--
ALTER TABLE `trx_bal`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `types`
--
ALTER TABLE `types`
  ADD PRIMARY KEY (`bet_type_id`),
  ADD UNIQUE KEY `sub_type_id` (`sub_type_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_bet_cancel`
--
ALTER TABLE `user_bet_cancel`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `bet_id` (`bet_id`);

--
-- Indexes for table `ux_categories`
--
ALTER TABLE `ux_categories`
  ADD PRIMARY KEY (`ux_id`),
  ADD UNIQUE KEY `competition_id` (`competition_id`),
  ADD KEY `sport_id` (`sport_id`),
  ADD KEY `category_id` (`category_id`);

--
-- Indexes for table `ux_todays_highlights`
--
ALTER TABLE `ux_todays_highlights`
  ADD PRIMARY KEY (`highlight_id`),
  ADD UNIQUE KEY `parent_match_id` (`parent_match_id`),
  ADD KEY `start_time` (`start_time`);

--
-- Indexes for table `virtual_competition`
--
ALTER TABLE `virtual_competition`
  ADD PRIMARY KEY (`v_competition_id`),
  ADD UNIQUE KEY `competition_name_category_sport_id` (`competition_name`,`category`,`sport_id`),
  ADD KEY `sport_id` (`sport_id`),
  ADD KEY `category_id` (`category_id`),
  ADD KEY `competition_name` (`competition_name`);

--
-- Indexes for table `virtual_event_odd`
--
ALTER TABLE `virtual_event_odd`
  ADD PRIMARY KEY (`v_event_odd_id`),
  ADD UNIQUE KEY `parent_match_id_sub_type_id_odd_key` (`parent_virtual_id`,`sub_type_id`,`odd_key`,`special_bet_value`),
  ADD KEY `match_id` (`parent_virtual_id`),
  ADD KEY `bet_type_id` (`sub_type_id`);

--
-- Indexes for table `virtual_latest_alive`
--
ALTER TABLE `virtual_latest_alive`
  ADD PRIMARY KEY (`alive_id`);

--
-- Indexes for table `virtual_match`
--
ALTER TABLE `virtual_match`
  ADD PRIMARY KEY (`v_match_id`),
  ADD UNIQUE KEY `parent_match_ids` (`parent_virtual_id`),
  ADD KEY `competition_id` (`competition_id`),
  ADD KEY `parent_match_id` (`parent_virtual_id`),
  ADD KEY `instance_id` (`instance_id`),
  ADD KEY `start_time` (`start_time`);

--
-- Indexes for table `virtual_odds`
--
ALTER TABLE `virtual_odds`
  ADD PRIMARY KEY (`live_odds_change_id`),
  ADD UNIQUE KEY `sub_type_ids` (`parent_match_id`,`sub_type_id`,`odd_key`,`special_bet_value`),
  ADD KEY `parent_match_id` (`parent_match_id`),
  ADD KEY `sub_type_id` (`sub_type_id`),
  ADD KEY `odd_key` (`odd_key`),
  ADD KEY `market_active` (`market_active`),
  ADD KEY `odd_active` (`odd_active`);

--
-- Indexes for table `virtual_odds_meta`
--
ALTER TABLE `virtual_odds_meta`
  ADD PRIMARY KEY (`live_odds_meta_id`),
  ADD UNIQUE KEY `parent_match_id_e` (`parent_match_id`),
  ADD KEY `parent_match_id` (`parent_match_id`),
  ADD KEY `betradar_timestamp` (`betradar_timestamp`),
  ADD KEY `sequence_no` (`sequence_no`),
  ADD KEY `event_status` (`event_status`),
  ADD KEY `match_time` (`match_time`),
  ADD KEY `bet_status` (`bet_status`);

--
-- Indexes for table `virtual_odd_type`
--
ALTER TABLE `virtual_odd_type`
  ADD PRIMARY KEY (`v_bet_type_id`),
  ADD UNIQUE KEY `sub_type_ids` (`sub_type_id`,`live_bet`);

--
-- Indexes for table `virtual_outcome`
--
ALTER TABLE `virtual_outcome`
  ADD PRIMARY KEY (`v_match_result_id`),
  ADD UNIQUE KEY `parent_match_id_sub_type_id_winning_outcome` (`parent_virtual_id`,`sub_type_id`,`winning_outcome`,`special_bet_value`),
  ADD KEY `sub_type_id` (`sub_type_id`),
  ADD KEY `parent_match_id` (`parent_virtual_id`),
  ADD KEY `winning_outcome` (`winning_outcome`);

--
-- Indexes for table `virtual_sport`
--
ALTER TABLE `virtual_sport`
  ADD PRIMARY KEY (`v_sport_id`),
  ADD UNIQUE KEY `constr_sport_name` (`sport_name`);

--
-- Indexes for table `void_bet_slip`
--
ALTER TABLE `void_bet_slip`
  ADD PRIMARY KEY (`bet_slip_id`),
  ADD KEY `bet_id` (`bet_id`),
  ADD KEY `parent_match_id` (`parent_match_id`),
  ADD KEY `bet_pick` (`bet_pick`);

--
-- Indexes for table `winner`
--
ALTER TABLE `winner`
  ADD PRIMARY KEY (`winner_id`),
  ADD KEY `bet_id` (`bet_id`),
  ADD KEY `profile_id` (`profile_id`);

--
-- Indexes for table `withdrawal`
--
ALTER TABLE `withdrawal`
  ADD PRIMARY KEY (`withdrawal_id`),
  ADD KEY `inbox_id` (`inbox_id`),
  ADD KEY `profile_id` (`msisdn`),
  ADD KEY `reference` (`reference`),
  ADD KEY `created` (`created`);

--
-- Indexes for table `withdraw_references`
--
ALTER TABLE `withdraw_references`
  ADD KEY `mpesa_ref` (`mpesa_ref`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `account_freeze`
--
ALTER TABLE `account_freeze`
  MODIFY `account_freeze_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT for table `airtel_money`
--
ALTER TABLE `airtel_money`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `airtel_money_rate`
--
ALTER TABLE `airtel_money_rate`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `airtel_subs_blast`
--
ALTER TABLE `airtel_subs_blast`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `android_auth`
--
ALTER TABLE `android_auth`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1180;

--
-- AUTO_INCREMENT for table `archive_bet`
--
ALTER TABLE `archive_bet`
  MODIFY `bet_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1175172;

--
-- AUTO_INCREMENT for table `archive_bet_slip`
--
ALTER TABLE `archive_bet_slip`
  MODIFY `bet_slip_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4973784;

--
-- AUTO_INCREMENT for table `archive_bonus_bet`
--
ALTER TABLE `archive_bonus_bet`
  MODIFY `bonus_bet_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27243;

--
-- AUTO_INCREMENT for table `archive_bonus_trx`
--
ALTER TABLE `archive_bonus_trx`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `archive_jackpot_bet`
--
ALTER TABLE `archive_jackpot_bet`
  MODIFY `jackpot_bet_id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `archive_jackpot_trx`
--
ALTER TABLE `archive_jackpot_trx`
  MODIFY `jackpot_trx_id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `archive_jackpot_winner`
--
ALTER TABLE `archive_jackpot_winner`
  MODIFY `jackpot_winner_id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `archive_match`
--
ALTER TABLE `archive_match`
  MODIFY `match_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47044610;

--
-- AUTO_INCREMENT for table `archive_transaction`
--
ALTER TABLE `archive_transaction`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `arch_live_match`
--
ALTER TABLE `arch_live_match`
  MODIFY `match_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `auth_token`
--
ALTER TABLE `auth_token`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8927;

--
-- AUTO_INCREMENT for table `backup_profile`
--
ALTER TABLE `backup_profile`
  MODIFY `profile_id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `bb`
--
ALTER TABLE `bb`
  MODIFY `bet_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `bet`
--
ALTER TABLE `bet`
  MODIFY `bet_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3806542;

--
-- AUTO_INCREMENT for table `betting_control`
--
ALTER TABLE `betting_control`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1344;

--
-- AUTO_INCREMENT for table `bet_discount`
--
ALTER TABLE `bet_discount`
  MODIFY `bet_discount_id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `bet_slip`
--
ALTER TABLE `bet_slip`
  MODIFY `bet_slip_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27340221;

--
-- AUTO_INCREMENT for table `bet_slip_check`
--
ALTER TABLE `bet_slip_check`
  MODIFY `bet_slip_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `bet_slip_temp`
--
ALTER TABLE `bet_slip_temp`
  MODIFY `bet_slip_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `bet_status_changes`
--
ALTER TABLE `bet_status_changes`
  MODIFY `status_change_id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `bleague_competition`
--
ALTER TABLE `bleague_competition`
  MODIFY `competition_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `bleague_event_odd`
--
ALTER TABLE `bleague_event_odd`
  MODIFY `event_odd_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `bleague_match`
--
ALTER TABLE `bleague_match`
  MODIFY `match_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `bonus_bet`
--
ALTER TABLE `bonus_bet`
  MODIFY `bonus_bet_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1662830;

--
-- AUTO_INCREMENT for table `bonus_bet_count`
--
ALTER TABLE `bonus_bet_count`
  MODIFY `bonus_bet_count_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `bonus_trx`
--
ALTER TABLE `bonus_trx`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2052857;

--
-- AUTO_INCREMENT for table `card_summary`
--
ALTER TABLE `card_summary`
  MODIFY `card_summary_id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5662392;

--
-- AUTO_INCREMENT for table `competition`
--
ALTER TABLE `competition`
  MODIFY `competition_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23300;

--
-- AUTO_INCREMENT for table `delivery_report`
--
ALTER TABLE `delivery_report`
  MODIFY `delivery_report_id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `event`
--
ALTER TABLE `event`
  MODIFY `match_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `event_odd`
--
ALTER TABLE `event_odd`
  MODIFY `event_odd_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2147483647;

--
-- AUTO_INCREMENT for table `free_bet`
--
ALTER TABLE `free_bet`
  MODIFY `free_bet_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `free_bet_transactions`
--
ALTER TABLE `free_bet_transactions`
  MODIFY `free_bet_transactions_id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `game`
--
ALTER TABLE `game`
  MODIFY `match_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `game_ids`
--
ALTER TABLE `game_ids`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22711;

--
-- AUTO_INCREMENT for table `game_request`
--
ALTER TABLE `game_request`
  MODIFY `request_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1258845;

--
-- AUTO_INCREMENT for table `gk_bonus_campaign`
--
ALTER TABLE `gk_bonus_campaign`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `gk_bonus_upload`
--
ALTER TABLE `gk_bonus_upload`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1001;

--
-- AUTO_INCREMENT for table `gk_sms`
--
ALTER TABLE `gk_sms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1001;

--
-- AUTO_INCREMENT for table `gk_users`
--
ALTER TABLE `gk_users`
  MODIFY `uid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `gr_profile_player`
--
ALTER TABLE `gr_profile_player`
  MODIFY `player_id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `gr_tickets`
--
ALTER TABLE `gr_tickets`
  MODIFY `gr_ticket_id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `inactive2_subs`
--
ALTER TABLE `inactive2_subs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `inactive_profile`
--
ALTER TABLE `inactive_profile`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `inbox`
--
ALTER TABLE `inbox`
  MODIFY `inbox_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=572560;

--
-- AUTO_INCREMENT for table `jackpot_bet`
--
ALTER TABLE `jackpot_bet`
  MODIFY `jackpot_bet_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=264;

--
-- AUTO_INCREMENT for table `jackpot_event`
--
ALTER TABLE `jackpot_event`
  MODIFY `jackpot_event_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=106;

--
-- AUTO_INCREMENT for table `jackpot_match`
--
ALTER TABLE `jackpot_match`
  MODIFY `jackpot_match_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=161;

--
-- AUTO_INCREMENT for table `jackpot_trx`
--
ALTER TABLE `jackpot_trx`
  MODIFY `jackpot_trx_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=264;

--
-- AUTO_INCREMENT for table `jackpot_type`
--
ALTER TABLE `jackpot_type`
  MODIFY `jackpot_type_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `jackpot_winner`
--
ALTER TABLE `jackpot_winner`
  MODIFY `jackpot_winner_id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jpbonus_award`
--
ALTER TABLE `jpbonus_award`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jp_bet_cancel_data`
--
ALTER TABLE `jp_bet_cancel_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `latest_alive`
--
ALTER TABLE `latest_alive`
  MODIFY `alive_id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `live_betting_control`
--
ALTER TABLE `live_betting_control`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `live_enabled_market`
--
ALTER TABLE `live_enabled_market`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `live_match`
--
ALTER TABLE `live_match`
  MODIFY `match_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29142;

--
-- AUTO_INCREMENT for table `live_meta_history`
--
ALTER TABLE `live_meta_history`
  MODIFY `live_odds_meta_id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `live_odds`
--
ALTER TABLE `live_odds`
  MODIFY `live_odds_change_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=96388970;

--
-- AUTO_INCREMENT for table `live_odds_change`
--
ALTER TABLE `live_odds_change`
  MODIFY `live_odds_change_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=593348;

--
-- AUTO_INCREMENT for table `live_odds_meta`
--
ALTER TABLE `live_odds_meta`
  MODIFY `live_odds_meta_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11050019;

--
-- AUTO_INCREMENT for table `logbetcancel`
--
ALTER TABLE `logbetcancel`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14638;

--
-- AUTO_INCREMENT for table `market_priority`
--
ALTER TABLE `market_priority`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `match`
--
ALTER TABLE `match`
  MODIFY `match_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48024812;

--
-- AUTO_INCREMENT for table `matchid`
--
ALTER TABLE `matchid`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `missed_withdrawals`
--
ALTER TABLE `missed_withdrawals`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `mobile_app_profile`
--
ALTER TABLE `mobile_app_profile`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1959;

--
-- AUTO_INCREMENT for table `mobile_app_version`
--
ALTER TABLE `mobile_app_version`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `mpesa_rate`
--
ALTER TABLE `mpesa_rate`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `mpesa_transaction`
--
ALTER TABLE `mpesa_transaction`
  MODIFY `mpesa_transaction_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5880011;

--
-- AUTO_INCREMENT for table `mts_exception`
--
ALTER TABLE `mts_exception`
  MODIFY `mts_exception_id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `mts_ticket_submit`
--
ALTER TABLE `mts_ticket_submit`
  MODIFY `mts_ticket_submit_id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `mts_validation_code`
--
ALTER TABLE `mts_validation_code`
  MODIFY `mts_validation_code_id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `network_charge_range`
--
ALTER TABLE `network_charge_range`
  MODIFY `network_charge_range_id` int(13) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `nums_pro`
--
ALTER TABLE `nums_pro`
  MODIFY `pro_id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `odds_history`
--
ALTER TABLE `odds_history`
  MODIFY `odd_history_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=888531799;

--
-- AUTO_INCREMENT for table `odds_subtype`
--
ALTER TABLE `odds_subtype`
  MODIFY `odds_subtype_id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `odd_history`
--
ALTER TABLE `odd_history`
  MODIFY `odd_history_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `odd_key_alias`
--
ALTER TABLE `odd_key_alias`
  MODIFY `odd_key_alias_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `odd_type`
--
ALTER TABLE `odd_type`
  MODIFY `bet_type_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1379985220;

--
-- AUTO_INCREMENT for table `odd_type_group`
--
ALTER TABLE `odd_type_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32078244;

--
-- AUTO_INCREMENT for table `odd_type_old`
--
ALTER TABLE `odd_type_old`
  MODIFY `bet_type_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1069798260;

--
-- AUTO_INCREMENT for table `outbox`
--
ALTER TABLE `outbox`
  MODIFY `outbox_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7165863;

--
-- AUTO_INCREMENT for table `outcome`
--
ALTER TABLE `outcome`
  MODIFY `match_result_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=505696540;

--
-- AUTO_INCREMENT for table `outcome_backup`
--
ALTER TABLE `outcome_backup`
  MODIFY `match_result_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=459953612;

--
-- AUTO_INCREMENT for table `outcome_o`
--
ALTER TABLE `outcome_o`
  MODIFY `match_result_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `outcome_old`
--
ALTER TABLE `outcome_old`
  MODIFY `match_result_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `outcome_test`
--
ALTER TABLE `outcome_test`
  MODIFY `match_result_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `outright`
--
ALTER TABLE `outright`
  MODIFY `outright_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `outright_competitor`
--
ALTER TABLE `outright_competitor`
  MODIFY `competitor_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `outright_odd`
--
ALTER TABLE `outright_odd`
  MODIFY `odd_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `outright_odd_history`
--
ALTER TABLE `outright_odd_history`
  MODIFY `odd_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `outright_outcome`
--
ALTER TABLE `outright_outcome`
  MODIFY `outcome_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `paybill_tariff`
--
ALTER TABLE `paybill_tariff`
  MODIFY `tariff_id` int(13) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `peer_bet`
--
ALTER TABLE `peer_bet`
  MODIFY `peer_bet_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=108;

--
-- AUTO_INCREMENT for table `prematch_disabled_market`
--
ALTER TABLE `prematch_disabled_market`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=88;

--
-- AUTO_INCREMENT for table `profile`
--
ALTER TABLE `profile`
  MODIFY `profile_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1069921;

--
-- AUTO_INCREMENT for table `profile_balance`
--
ALTER TABLE `profile_balance`
  MODIFY `profile_balance_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6115654;

--
-- AUTO_INCREMENT for table `profile_bonus`
--
ALTER TABLE `profile_bonus`
  MODIFY `profile_bonus_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=633578;

--
-- AUTO_INCREMENT for table `profile_setting`
--
ALTER TABLE `profile_setting`
  MODIFY `profile_setting_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `profile_settings`
--
ALTER TABLE `profile_settings`
  MODIFY `profile_setting_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=657448;

--
-- AUTO_INCREMENT for table `running_balance`
--
ALTER TABLE `running_balance`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `scorepesa_point`
--
ALTER TABLE `scorepesa_point`
  MODIFY `shikabet_point_id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `scorepesa_point_bet`
--
ALTER TABLE `scorepesa_point_bet`
  MODIFY `shikabet_point_bet_id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `scorepesa_point_summary`
--
ALTER TABLE `scorepesa_point_summary`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `scorepesa_point_trx`
--
ALTER TABLE `scorepesa_point_trx`
  MODIFY `shikabet_point_trx_id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `scorepesa_profile_names`
--
ALTER TABLE `scorepesa_profile_names`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `seven_aggregator_request`
--
ALTER TABLE `seven_aggregator_request`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `shop_deposits`
--
ALTER TABLE `shop_deposits`
  MODIFY `transaction_id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `shop_withdrawals`
--
ALTER TABLE `shop_withdrawals`
  MODIFY `withdrawal_id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `speed_dial_history`
--
ALTER TABLE `speed_dial_history`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `speed_dial_profile`
--
ALTER TABLE `speed_dial_profile`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sport`
--
ALTER TABLE `sport`
  MODIFY `sport_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=167;

--
-- AUTO_INCREMENT for table `talksport`
--
ALTER TABLE `talksport`
  MODIFY `talksport_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `team`
--
ALTER TABLE `team`
  MODIFY `team_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ticket_settlement`
--
ALTER TABLE `ticket_settlement`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1008;

--
-- AUTO_INCREMENT for table `tournament`
--
ALTER TABLE `tournament`
  MODIFY `profile_balance_id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `traffic_analytic`
--
ALTER TABLE `traffic_analytic`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `transaction`
--
ALTER TABLE `transaction`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10799149;

--
-- AUTO_INCREMENT for table `trx_bal`
--
ALTER TABLE `trx_bal`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1081805;

--
-- AUTO_INCREMENT for table `types`
--
ALTER TABLE `types`
  MODIFY `bet_type_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;

--
-- AUTO_INCREMENT for table `user_bet_cancel`
--
ALTER TABLE `user_bet_cancel`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ux_categories`
--
ALTER TABLE `ux_categories`
  MODIFY `ux_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `ux_todays_highlights`
--
ALTER TABLE `ux_todays_highlights`
  MODIFY `highlight_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `virtual_competition`
--
ALTER TABLE `virtual_competition`
  MODIFY `v_competition_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `virtual_event_odd`
--
ALTER TABLE `virtual_event_odd`
  MODIFY `v_event_odd_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `virtual_latest_alive`
--
ALTER TABLE `virtual_latest_alive`
  MODIFY `alive_id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `virtual_match`
--
ALTER TABLE `virtual_match`
  MODIFY `v_match_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `virtual_odds`
--
ALTER TABLE `virtual_odds`
  MODIFY `live_odds_change_id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `virtual_odds_meta`
--
ALTER TABLE `virtual_odds_meta`
  MODIFY `live_odds_meta_id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `virtual_odd_type`
--
ALTER TABLE `virtual_odd_type`
  MODIFY `v_bet_type_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `virtual_outcome`
--
ALTER TABLE `virtual_outcome`
  MODIFY `v_match_result_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `virtual_sport`
--
ALTER TABLE `virtual_sport`
  MODIFY `v_sport_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `void_bet_slip`
--
ALTER TABLE `void_bet_slip`
  MODIFY `bet_slip_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6919;

--
-- AUTO_INCREMENT for table `winner`
--
ALTER TABLE `winner`
  MODIFY `winner_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `withdrawal`
--
ALTER TABLE `withdrawal`
  MODIFY `withdrawal_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=166288;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `auth_assignment`
--
ALTER TABLE `auth_assignment`
  ADD CONSTRAINT `auth_assignment_ibfk_1` FOREIGN KEY (`item_name`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `auth_item`
--
ALTER TABLE `auth_item`
  ADD CONSTRAINT `auth_item_ibfk_1` FOREIGN KEY (`rule_name`) REFERENCES `auth_rule` (`name`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `auth_item_child`
--
ALTER TABLE `auth_item_child`
  ADD CONSTRAINT `auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `bet`
--
ALTER TABLE `bet`
  ADD CONSTRAINT `bet_ibfk_2` FOREIGN KEY (`profile_id`) REFERENCES `profile` (`profile_id`);

--
-- Constraints for table `bet_slip`
--
ALTER TABLE `bet_slip`
  ADD CONSTRAINT `bet_slip_ibfk_22` FOREIGN KEY (`bet_id`) REFERENCES `bet` (`bet_id`);

--
-- Constraints for table `bonus_bet`
--
ALTER TABLE `bonus_bet`
  ADD CONSTRAINT `bonus_bet_ibfk_1` FOREIGN KEY (`bet_id`) REFERENCES `bet` (`bet_id`);

--
-- Constraints for table `bonus_bet_count`
--
ALTER TABLE `bonus_bet_count`
  ADD CONSTRAINT `bonus_bet_count_ibfk_1` FOREIGN KEY (`profile_id`) REFERENCES `profile` (`profile_id`);

--
-- Constraints for table `free_bet_transactions`
--
ALTER TABLE `free_bet_transactions`
  ADD CONSTRAINT `free_bet_transactions_ibfk_1` FOREIGN KEY (`free_bet_id`) REFERENCES `free_bet` (`free_bet_id`);

--
-- Constraints for table `gr_profile_player`
--
ALTER TABLE `gr_profile_player`
  ADD CONSTRAINT `gr_profile_player_ibfk_2` FOREIGN KEY (`profile_id`) REFERENCES `profile` (`profile_id`);

--
-- Constraints for table `gr_tickets`
--
ALTER TABLE `gr_tickets`
  ADD CONSTRAINT `gr_tickets_ibfk_2` FOREIGN KEY (`profile_id`) REFERENCES `profile` (`profile_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `jackpot_bet`
--
ALTER TABLE `jackpot_bet`
  ADD CONSTRAINT `jackpot_bet_ibfk_3` FOREIGN KEY (`bet_id`) REFERENCES `bet` (`bet_id`),
  ADD CONSTRAINT `jackpot_bet_ibfk_4` FOREIGN KEY (`jackpot_event_id`) REFERENCES `jackpot_event` (`jackpot_event_id`);

--
-- Constraints for table `jackpot_event`
--
ALTER TABLE `jackpot_event`
  ADD CONSTRAINT `jackpot_event_ibfk_1` FOREIGN KEY (`jackpot_type`) REFERENCES `jackpot_type` (`jackpot_type_id`);

--
-- Constraints for table `jackpot_trx`
--
ALTER TABLE `jackpot_trx`
  ADD CONSTRAINT `jackpot_trx_ibfk_1` FOREIGN KEY (`jackpot_event_id`) REFERENCES `jackpot_event` (`jackpot_event_id`);

--
-- Constraints for table `jackpot_winner`
--
ALTER TABLE `jackpot_winner`
  ADD CONSTRAINT `jackpot_winner_ibfk_1` FOREIGN KEY (`bet_id`) REFERENCES `bet` (`bet_id`),
  ADD CONSTRAINT `jackpot_winner_ibfk_2` FOREIGN KEY (`jackpot_event_id`) REFERENCES `jackpot_event` (`jackpot_event_id`);

--
-- Constraints for table `match`
--
ALTER TABLE `match`
  ADD CONSTRAINT `match_ibfk_1` FOREIGN KEY (`competition_id`) REFERENCES `competition` (`competition_id`);

--
-- Constraints for table `menu`
--
ALTER TABLE `menu`
  ADD CONSTRAINT `menu_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `menu` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `paybill_tariff`
--
ALTER TABLE `paybill_tariff`
  ADD CONSTRAINT `fk_paybill_tariff_1` FOREIGN KEY (`network_range_id`) REFERENCES `network_charge_range` (`network_charge_range_id`);

--
-- Constraints for table `profile_balance`
--
ALTER TABLE `profile_balance`
  ADD CONSTRAINT `profile_balance_fk1` FOREIGN KEY (`profile_id`) REFERENCES `profile` (`profile_id`);

--
-- Constraints for table `profile_bonus`
--
ALTER TABLE `profile_bonus`
  ADD CONSTRAINT `profile_bonus_ibfk_1` FOREIGN KEY (`profile_id`) REFERENCES `profile` (`profile_id`);

--
-- Constraints for table `transaction`
--
ALTER TABLE `transaction`
  ADD CONSTRAINT `transaction_ibfk_1` FOREIGN KEY (`profile_id`) REFERENCES `profile` (`profile_id`);

--
-- Constraints for table `virtual_event_odd`
--
ALTER TABLE `virtual_event_odd`
  ADD CONSTRAINT `v_event_odd_fk_1` FOREIGN KEY (`parent_virtual_id`) REFERENCES `virtual_match` (`parent_virtual_id`),
  ADD CONSTRAINT `v_event_odd_fk_2` FOREIGN KEY (`sub_type_id`) REFERENCES `virtual_odd_type` (`sub_type_id`);

--
-- Constraints for table `virtual_match`
--
ALTER TABLE `virtual_match`
  ADD CONSTRAINT `v_match_ibfk_1` FOREIGN KEY (`competition_id`) REFERENCES `virtual_competition` (`v_competition_id`);

--
-- Constraints for table `virtual_outcome`
--
ALTER TABLE `virtual_outcome`
  ADD CONSTRAINT `v_outcome_fk_1` FOREIGN KEY (`parent_virtual_id`) REFERENCES `virtual_match` (`parent_virtual_id`);

--
-- Constraints for table `winner`
--
ALTER TABLE `winner`
  ADD CONSTRAINT `winner_ibfk_1` FOREIGN KEY (`bet_id`) REFERENCES `bet` (`bet_id`),
  ADD CONSTRAINT `winner_ibfk_2` FOREIGN KEY (`profile_id`) REFERENCES `profile` (`profile_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
